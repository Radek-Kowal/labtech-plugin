let ARTICLES = [];
let ADDONARTICLESGROUPID;
let SELECTED_ADDON_GROUP_ITEM;
let SELECTED_PARENT_ARTICLE;
let ADDONS_DETAILS={};
let ORIG_ITEM;

$(function () {

	// Create a new menu entry in the top navigation menu
	var li = $('<li/>').html("<a href=\"javascript: void(0);\">Configure Plugin</a>").on("click", function () {
			showSpecialUI();
		});
	$('#navigation').append(li);
});

function showSpecialUI() {
	// Show the special IU

	// This will hide all components on the sales screen
	$('#mainView').hide();

	// Get or create the special ui
	var specialUI = $('#specialUI');
	if (specialUI == null || specialUI.length == 0) {
		specialUI = menuMBButtons();
	}
	// Show it
	specialUI.show();

	// add more HTML to the page
	addCustomHTMLContent();
}

function addCustomHTMLContent() {
	addOnsManagementUI();
	discountsManagementUI();
}

function hideSpecialUI() {
	// Hide the special ui
	var specialUI = $('#specialUI');
	specialUI.hide();

	// Display the normal sales screen again
	$('#mainView').show();
}

function menuMBButtons() {
	// Creation of the menu main div
	var specialUIContainer = $('<div/>').attr("id", "menuUI").css({
			"padding" : "50px"
		});
	$('#mainView').before(specialUIContainer);

	var containerDiv = $('<div/>').css({
			"width" : "65%",
			"height" : "auto"
		});

	specialUIContainer.append(containerDiv);

	let buttons = `<div class="widget">
			<h1 style='padding-bottom: 10px'>Plugin Config</h1>
			<button id='addons'>Addons</button>
			<button id='discounts'>Discounts</button>
			<button id='inventory'>Inventory</button>
			<button id='service'>Service charge</button>
		</div>`;

	containerDiv.append(buttons);

	$(function () {
		$(".widget input[type=submit], .widget a, .widget button").button();
		$("button#addons").click(function (event) {
			showDiscountsManagementUI(false);
			showAddInventoryManagementUI(false);
			showServiceManagementUI(false);
			showAddOnsManagementUI(true);
		});
		
		$("button#discounts").click(function (event) {
			showAddOnsManagementUI(false);
			showAddInventoryManagementUI(false);
			showServiceManagementUI(false);
			showDiscountsManagementUI(true);
		});
		
		$("button#inventory").click(function (event) {
			showAddOnsManagementUI(false);
			showDiscountsManagementUI(false);
			showServiceManagementUI(false);
			showAddInventoryManagementUI(true);
		});
		
		$("button#service").click(function (event) {
			showAddOnsManagementUI(false);
			showDiscountsManagementUI(false);
			showAddInventoryManagementUI(false);
			showServiceManagementUI(true);
		});
	});

	return specialUIContainer;
}

function showAddOnsManagementUI(turnOn) {
	if (turnOn) {
		clearGlobalAddonItems();
		
		$('#tabs-1').show();
		clearArticlesGroupsGrid();
		clearArticlesAddonsGrid();
		 
		$("#addArticleToGroup").hide();
		$("#editArticleToGroup").hide();
		$("#deleteArticleToGroup").hide();
		loadArticlesOptions();
		createAddonsGroupsGrid();
	} else {
		$('#tabs-1').hide();
	}
}

function showDiscountsManagementUI(turnOn) {
	if (turnOn) {
		$('#tabs-4').show();
		loadArticlesOptions();
	} else {
		$('#tabs-4').hide();
	}
}

function showAddInventoryManagementUI(turnOn) {
	if (turnOn) {
		$('#tabs-6').show();
		loadArticlesOptions();
	} else {
		$('#tabs-6').hide();
	}
}

function showServiceManagementUI(turnOn) {
	if (turnOn) {
		$('#tabs-8').show();
		loadArticlesOptions();
	} else {
		$('#tabs-8').hide();
	}
}

function loadArticlesOptions() {
//	clearArticlesGroupsGrid();
	showInProgressDialog();

	$.ajax({
		"url" : "PluginServlet?action=getArticles",
		"dataType" : "JSON",
		"type" : "GET"
	}).success(function (data) {
		hideInProgressDialog();
		console.log(data);
		
		ARTICLES = data.rows;

		// since we found it -> remove option first
		$("#articleSelectBox option").each(function () {
			$(this).remove();
		});

		// add extra empty option
		$('#articleSelectBox').append($('<option>', {
				value : 0,
				text : "Please choose article"
			}));

		// add loaded options
		$.each(data.rows, function (i, item) {
			$('#articleSelectBox').append($('<option>', {
					value : item.itemCode,
					text : item.itemCode
				}));
		});		
	}).fail(function () {
		hideInProgessDialog();
		alert("Error!");
	});
}

function addOnsManagementUI() {
	var dialogAddOnsManagementUIHtml = `
		<div id="tabs-1" style="display: none; width: 75%; margin-top: 5px;">
			<!-- <button id="sendEmail">Send email</button> -->
	        <ul>
	            <li>
	                <a href="#tabs-2">Addons</a>
	            </li>
	            <li>
	                <a href="#tabs-3">Addon groups</a>
	            </li>
	        </ul>
	        <div id="tabs-2">
	            <div id="addonConfigurationContainer">
	                <fieldset class="addonConfigurationFieldset">
	                    <legend>Addon Details</legend>
	                    <form id="addonConfigurationForm" enctype="utf-8">
	                        <div id="column">
	                            <div id="inputFieldContainer">
		                            <div id="articles">
		                            	<label for="articleSelectBox" class="left selectBoxLabel">Parent article
		                                    <select name="articleSelectBox" id="articleSelectBox" class="text ui-widget-content ui-corner-all" />
		                                </label>
		                                <label for="articleDescription" class="left">Description
		                                    <input type="text" name="articleDescription" id="articleDescription" class="text ui-widget-content ui-corner-all" readonly style="padding-left: 3px;">
		                                </label>
		                                <label for="articlePrice" class="left">Price
		                                    <input type="text" name="articlePrice" id="articlePrice" class="text ui-widget-content ui-corner-all" readonly style="padding-left: 3px;">
		                                </label>		                                
		                            </div>	                                
	                                <div id="addonGroupsForArticleGrid">
	                                	<table id="articleGroupsGrid" />
	                                	<div id="articleGroupsPager" />
	                                	<button id="addArticleToGroup">Add to Group</button>
						                <button id="editArticleToGroup">Edit Group</button>
						                <button id="deleteArticleToGroup">Delete Group</button>
	                                </div>
	                                <div id="addonsForArticleGrid">
	                                	<table id="articleAddonsGrid" />
	                                	<div id="articleAddonsPager" />
	                                	<button id="addAddonToArticle" style="display: none;">Add to Article</button>
						                <button id="editAddonToArticle" style="display: none;">Edit Addon</button>
						                <button id="deleteAddonToArticle" style="display: none;">Delete Addon</button>
	                                </div>
	                            </div>
	                        </div>
	                    </form>	                    
	                </fieldset>
	            </div>
	        </div>
	        <div id="tabs-3">
                <div id="jqgridAddonGroups">
                    <table id="addonGroupsGrid" />
                    <div id="addonGroupsPager" />
                </div>
                <button id="add-new-row">Add Group</button>
                <button id="edit-row">Edit Group</button>
                <button id="delete-row">Delete Group</button>
	        </div>
		</div>`;

	$("#menuUI").append(dialogAddOnsManagementUIHtml);

	$(function () {
		$("#tabs-1").tabs({
			show: function( event, ui ) {
				console.log(ui);
				if (ui.index == 0) {
					clearArticlesGroupsGrid();
				}
			}
		});
	});
	

	// load input values after selection
	$("#articleSelectBox").change(function () {
		clearArticlesAddonsGrid();
		
		let itemCode = $(this).val();
		if (itemCode === "0") {
			clearArticlesGroupsGrid();
			return;
		}
		
		SELECTED_PARENT_ARTICLE = ARTICLES.find((e) => { return e.itemCode === itemCode; });
		let itemName = (SELECTED_PARENT_ARTICLE) ? SELECTED_PARENT_ARTICLE.itemName : "";
		let price = (SELECTED_PARENT_ARTICLE) ? SELECTED_PARENT_ARTICLE.unitPrice : 0;
		$("#articleDescription").val(itemName);
		$("#articlePrice").val(price);
		
		articlesGroupsModule.resetGrid();
//		articlesGroupsModule.createArticleGroupsGrid(itemCode);
	});
}

function clearArticlesGroupsGrid() {
	$('#articleSelectBox').prop('selectedIndex',0);
	$("#articleDescription").val("");
	$("#articlePrice").val("");
	
	articlesGroupsModule.resetGrid();
}

function clearArticlesAddonsGrid() {
	$("#articleAddonsGrid").jqGrid('GridUnload');
	$("#addAddonToArticle").hide();
	$("#editAddonToArticle").hide();
	$("#deleteAddonToArticle").hide();
	
	articleAddonsModule.resetGrid();
}

function discountsManagementUI() {
	var dialogDiscountsManagementUIHtml = `
		<div id="tabs-4" style="display: none; width: 75%; margin-top: 5px;">
	        <ul>
	            <li>
	                <a href="#tabs-5">Discounts</a>
	            </li>
	        </ul>
		</div>`;

	$("#menuUI").append(dialogDiscountsManagementUIHtml);

	$(function () {
		$("#tabs-4").tabs();
	});
}

function showBackendCallStatus(data, messageOnSuccess) {
		if (data["status"] === "success") {  					
		showMessageDialog(messageOnSuccess, {
			"type" : "info"
		});
	} else {
		showMessageDialog("Error in backend call: " + data["message"], {
			"type" : "error"
		});	
	}
}

function clearGlobalAddonItems() {
	SELECTED_ADDON_GROUP_ITEM = undefined;
	SELECTED_PARENT_ARTICLE = undefined;
	ADDONARTICLESGROUPID = undefined;
}

/*
 * This function is called when a new sales item should be added. This is the
 * case, for example, if a string was entered in the material input on the sales
 * screen
 */
var queryForSalesItemOrig = queryForSalesItem;

queryForSalesItem = function(item) {
	setInBackendCall(false);
	// check if parent item
	ORIG_ITEM=item;
	addonClientUIModule.getArticleInfo(item);
	
	queryForSalesItemOrig(item);
	// debug - check what's logged
	console.log("queryForSalesItem: " + JSON.stringify(item));
};