/*!
 * custom styles for JQGrid
 *
 */
$(`<style type="text/css">	
	fieldset.addonConfigurationFieldset {
		height: auto;
		width: 85%;
	}
	div#jqgridAddonGroups input, div#jqgridAddonGroups select {
		width: auto;
	}
	
	div#addonGroupsForArticleGrid {
		width: 80%;
		height: auto;
		padding: 0 0 10px 0;
	}
	
	div#articleGroupsPager, div#addonsForArticleGrid {
		padding-bottom: 5px;
	}
	
	div#editmodarticleGroupsGrid {
		width: auto;
	}
	
	div#addonGroupsForArticleGrid input, div#addonGroupsForArticleGrid select {
		width: auto;
	}
	
	div#articles {
		height: 150px;
	}
	
	div#addonsForArticleGrid input, div#addonsForArticleGrid select {
		width: auto;
	}
	
	.ui-state-highlight, .ui-widget-content .ui-state-highlight, .ui-widget-header .ui-state-highlight {
		border: 3px solid #fc4913;
		background: #cee2e1 50% 50% repeat-x;
		color: #915608;
	}
	
	#dialogAddOnsClientUI * {
        box-sizing: border-box;
    }
    
    #dialogAddOnsClientUI {
        height: 650px;
        width: 80%;
        border: 5px solid darkgrey;
        overflow: auto;
    }
    
    .table {
        display: table;
        width: 100%;
    }
    
    .header {
        height: 150px;
        align-items: center;
        width: 100%;
    }
    
    .cell {
        display: table-cell;
        padding: 1em;
        vertical-align: middle;
        margin-right: 30px;
    }
    
    div#title {
        font-size: 24px;
        height: 60px;
        font-weight: 700;
    }
    
    div#parent {
        display: table;
        color: black;
        font-size: 20px;
    }
    
    #parent div {
        padding: 5px 5px 5px 5px;
        margin-right: 10px;
        /* width: 250px; */ 
    }
    
    .parent_row_1, .parent_row_2 {
        display: table-row;
    }
    
    div.parent_row_2#parent_name {
		font-weight: bold;
    }
    
    div.parent_row_2#parent_id {
		font-weight: bold;
    }
    
    div#parent_id {
        margin-bottom: 5px;
    }
    
    div#parent_name {
        margin-bottom: 5px;
    }
    
    div.button_no_addons {
        color: white;
        font-size: 20px;
        background: red;
        border: 3px solid darkgrey;
        border-radius: 3px;
        width: 150px;
        height: 100px;
        text-align: center;
        display: table-cell;
        vertical-align: middle;
        margin: 0 auto;
    }
    
    div.button_addons_add {
        color: white;
        font-size: 20px;
        background: green;
        border: 3px solid darkgrey;
        border-radius: 3px;
        width: 150px;
        height: 100px;
        text-align: center;
        display: table-cell;
        vertical-align: middle;
        margin: 0 auto;
    }
    
    .addons_container {
        display: flex;
        flex-direction: column;
        flex: 1;
    }
    
    .group {}
    
    .addons {
        display: flex;
        flex-wrap: wrap;
        flex: 1;
    }
    
    .addon {
        width: 200px;
        border: 1px solid;
        border-radius: 3px;
        margin: 10px;
        background: #e5f6f6;
    }
    
    .addon_row {
        width: 100%;
        padding: .5em;
    }
    
    .addon_cell {
        float: left;
    }
    
    .group_title,
    .group_footer {
        font-size: 20px;
        font-weight: 600;
        padding: 1em;
        height: 45px;
    }
    
    .addon_name {
        text-align: center;
        font-size: 16px;
        font-weight: bold;
    }
    
    #addon_quantity_title,
    #addon_price_title {
        width: 50%;
        padding: .5em;
    }
    
    #addon_quantity_value,
    #addon_price_value {
        width: 50%;
        text-align: right;
        padding: .5em;
    }
    
    .addon_cell input {
        width: 100%;
    }
    
    .addon_highlight {
        background: #e3e6f6;
        font-weight: bold;
    }
    
    div.addon_button_hover {
		border: 3px solid black;
		color: black;
		cursor: pointer;
    }
    
    .addon_selected {
	    background: darkgreen;
	    color: white;
	    border: 3px solid red;
    }
}
</style>`).appendTo("head");