/*!
 * handle jQgrid ArticleGropus grid
 *
 */

let articlesGroupsModule = (function() {
    'use strict';
 
    let allData = [];
    let addonGroupsForSelect = {};
    let sortOrderForSelect = {};
    
    function gridReload() {
    	$("#articleGroupsGrid").jqGrid("clearGridData");
    	$("#articleGroupsGrid").jqGrid("setGridParam", {data: allData});
    	$("#articleGroupsGrid").trigger("reloadGrid");
    	
    	getSortOrder(allData);
    	var cm = $('#articleGroupsGrid').jqGrid('getColProp',"groupName");
    	console.log(cm);
    }
    
    function resetGrid() {
        // reset the "value" property of the editoptions property to the initial value
    	let article = $("#articleSelectBox").val() || "";
		$("#articleGroupsGrid").jqGrid('GridUnload');
		createArticleGroupsGrid(article);
    }
  	
  	function getArticleGroupsFromServer(itemCode) {
  		$.ajax({
  			type :"GET",
  		    contentType :"application/json; charset=utf-8",
  		    dataType :"json",
  		    url: "PluginServlet?action=getArticleGroups&itemCode=" + itemCode,
  		    async: false
  		}).success((data) => {
  			hideInProgessDialog();
  			
  			allData = data.rows;
  			gridReload();
  		}).fail((xhr, status, error) => {
  			hideInProgessDialog();
  			showMessageDialog("Error in backend call: " + + xhr.responseText, {
  				"type" : "error"
  			});		
  		});
  	}
  	
  	function getAddonGroupsFromServer() {
  		$.ajax({
  			type :"GET",
  		    contentType :"application/json; charset=utf-8",
  		    dataType :"json",
  		    url : "PluginServlet?action=getAddonGroups",
  		    async: false
  		}).success((data) => {
  			hideInProgessDialog();
  			if (data && data.rows && data.rows.length > 0) {
  				data.rows.forEach(prepareGroups);
  			}  			
  		}).fail((xhr, status, error) => {
  			hideInProgessDialog();
  			showMessageDialog("Error in backend call: " + xhr.responseText, {
  				"type" : "error"
  			});		
  		});
  	}
  	
  	function prepareGroups(element, index, array) {
  		addonGroupsForSelect[element["groupId"].toString()] = element["groupName"];
  		console.log("addonGroupsForSelect: ");
  		console.log(JSON.stringify(addonGroupsForSelect));
  	}
  	
  	function getSortOrder(data) {
  		if (data && data.length > 0) {
  			sortOrderForSelect[1] = data.length + 1;
  		} else {
  			sortOrderForSelect[1] = 1;
  		}
  	}
  	
  	function createArticleGroupsGrid(itemCode) {
  		// show the buttons
  		$("#addArticleToGroup").show();
  		$("#editArticleToGroup").show();
  		$("#deleteArticleToGroup").show();
  		
  		let groupIdForDelUpdate = "0";
  		
  		getAddonGroupsFromServer();
  		console.log(addonGroupsForSelect);
  		
  		getArticleGroupsFromServer(itemCode);
  		console.log(allData);
  		
  		let idForUpdate, oldSortOrder, newSortOrder;

  		let lastSel;
  		var grid = $("#articleGroupsGrid");
  		// Prepare jQgrid
  		$("#articleGroupsGrid").jqGrid({
  			data: allData,
  			datatype: "local",
  			colNames : ["Id", 
  				"Group Id", 
  				"Addon Group",
  				"Group Sort Order",
  				"Single or Multi Select"],
  			colModel : [{
  					name : "id",
  					index: "id",
  					width : 35,
  					editable : false,
  					hidden: true
  				},{
  					name : "addonGroupId",
  					index: "addonGroupId",
  					hidden : true,
  					editable: false
  				}, {
  					name : "groupName",
  					index: "groupName",
  					width : 90,
  					editable: false
  				},{
  					name : "sortOrder",
  					index: "sortOrder",
  					width : 50,
  					editable: true  					
  				}, {
  					name : "single",
  					index :"single",
  	                align : "center",
  	                width : 35,
  	                editable : true,
  	                cellEdit : true,
  	                edittype : "select", 
  	                formatter : "select",

  	                editoptions:{value: getSingleSelectOptions()}
  				}
  			],
  			beforeSelectRow: function(id) {
				var cm = $('#articleGroupsGrid').jqGrid('getColProp',"groupName");
				cm.editable = true;// Editable
				cm.edittype = "select";
				cm.formatter = "select";
				cm.editoptions = {
					aysnc: false, 
					dataUrl: "PluginServlet?action=getAddonGroups",
					buildSelect: function (data) {  
						var responseJSON = $.parseJSON(data);
						var s = "<select>";
						s += '<option value="0">--No Group--</option>';
						responseJSON.rows.map((o) => {
							console.log(o.groupId);
							s += '<option value="' + o.groupId + '">' + o.groupName + '</option>';
						});
						return s + "</select>";
					}
				}
				return true;
			}, 
  			onSelectRow: (rowid,status,e) => {
  				let rowData = $("#articleGroupsGrid").jqGrid("getRowData",parseInt(rowid,10));
  				SELECTED_ADDON_GROUP_ITEM = "";
  				ADDONARTICLESGROUPID = rowData["id"];
  				SELECTED_ADDON_GROUP_ITEM = (rowData["addonGroupId"]) ? rowData["addonGroupId"] : "";
  				articleAddonsModule.createArticleAddonsGrid(ADDONARTICLESGROUPID);
  			},
  			rowNum : 10,
  			rowList : [10, 20, 30],
  			autowidth : false,
  			width : 600,
  			rownumbers : true,
  			pager : "#articleGroupsPager",
  			sortname : "sortOrder",
  			viewrecords : true,
  			sortorder : "asc",
  			caption : "Article Groups",
  			emptyrecords : "No Groups",
  			loadonce : false,
  			toolbar: [true,"top"],
  			jsonReader : {
  				root : "rows",
  				page : "page",
  				total : "total",
  				records : "records",
  				repeatitems : false,
  				cell : "cell",
  				id : "id"
  			}
  		});	
  		
  		$("#articleGroupsGrid").sortableRows();   
  		$("#articleGroupsGrid").jqGrid("gridDnD");
  		
  		function getSingleSelectOptions(){
  			var options = { "Y": "Yes", "N": "No" };
  			return options;
  		}
  		
  		let jsonAddArticleGroupOptions = {
  		    type :"POST",
  		    contentType :"application/json; charset=utf-8",
  		    dataType :"json",
  		    "url": "PluginServlet?action=addArticleToGroup",
  		    "async": false,
  			"success": function(data){
  				hideInProgessDialog(); 	
  				showBackendCallStatus(data, "New group for article added successfully!");
  			},
  			"error": function(xhr, status, error){
  				hideInProgessDialog();
  				showMessageDialog("Error in backend call: " + xhr.responseText, {
  					"type" : "error"
  				});	
  			}
  		};
  		
  		let jsonEditGroupOptions = {
  		    type :"POST",
  		    contentType :"application/json; charset=utf-8",
  		    dataType :"json",
  		    "url": "PluginServlet?action=editArticleToGroup",
  		    "async": false,
  			"success": function(data){
  				hideInProgessDialog(); 	
  				showBackendCallStatus(data, "Article to Group assigment edited successfully!");  				
  			},
  			"error": function(xhr, status, error){
  				hideInProgessDialog();
  				showMessageDialog("Error in backend call: " + xhr.responseText, {
  					"type" : "error"
  				});	
  			}
  		};

  		let editArticleFormOptions = {
  			height:200,
  			width: 400,
  			processData: "Processing...",
  			ajaxEditOptions: jsonEditGroupOptions,
  			serializeEditData: (postdata) => {
  				let rowid = $("#articleGroupsGrid").jqGrid("getGridParam","selrow");
  				let output = [];
  	  			if( rowid != null ) {
  	  				let rowData = $("#articleGroupsGrid").jqGrid("getRowData",parseInt(rowid,10));
  	  				idForUpdate = rowData["id"];
  	  				
  	  				let itemCode = $("#articleSelectBox").val();
  	  				let groupId = $("#groupName").val();
  	  				
  	  				oldSortOrder = rowData["sortOrder"];
  	  				newSortOrder = postdata.sortOrder || oldSortOrder;
  	  				
  	  				let addonSingle = $("#single").val();
  	  				
  	  				output.push({	
  	  					id: idForUpdate,
	  	  				parentArticle: itemCode,
	  			    	groupId: groupId,
	  			    	sortOrder: newSortOrder,
	  			    	oldSortOrder: oldSortOrder,
	  			    	addonSingle: postdata.single
  	  				});
  	  			}
  				 return JSON.stringify(output);
  			},
  			recreateForm : true,
  			closeAfterEdit : true,
  			reloadAfterSubmit : true,
  			afterSubmit: (response, postdata) => {
  				console.log("IN afterSubmit for editArticleFormOptions");
  				return [true,"",null];
  			},
  			onClose: function () {
  				resetGrid();
            }
  		}
  		
  		let addArticleFormOptions = {
  			addCaption: "Add Group",
  			width: "auto",
  			height:200,
  			processData: "Processing...",
  			ajaxEditOptions: jsonAddArticleGroupOptions,
  			serializeEditData: (postdata) => {
  				let itemCode = $("#articleSelectBox").val();
  				let groupId = $("#groupName").val();
  				let sortOrder = sortOrderForSelect[1];
  				let output = [];
  			    output.push({	    	
  			    	parentArticle: itemCode,
  			    	groupId: groupId,
  			    	sortOrder: sortOrder,
  			    	addonSingle: $("#single").val()
  			    });
  			    return JSON.stringify(output);
  			},
  			recreateForm : true,
  			closeAfterAdd : true,
  			reloadAfterSubmit : true,
  			beforeShowForm: (formid) => {
  				setGroupNameColumnProperties();  				
  				setSortOrderColumnProperties(); 
  			},
  			afterSubmit: (response, postdata) => {
  				console.log("IN afterSubmit for addArticleFormOptions");
  				return [true,"",null];
  			},
  			onClose: function () {
  				resetGrid();
            }
  		}
  		
  		$("#deleteArticleToGroup").click( (e) => {
  			e.preventDefault();
  			let rowid = $("#articleGroupsGrid").jqGrid("getGridParam","selrow");
  			if( rowid != null ) {
  				let rowData = $("#articleGroupsGrid").jqGrid("getRowData",parseInt(rowid,10));
  				let iddForDelUpdate = rowData["id"];
  				showMessageDialog("Delete selected record?", {
  					"type" : "warning",
  					"dialogId" : "confirmArticleToGroupDelete",
  					"buttons" : [ {
  						"id" : "add",
  						"type" : "good",
  						"text" : "Remove",
  						"callback" : () => {
  							deleteArticleToGroupOnServer(iddForDelUpdate);	
  							$("#articleGroupsGrid").trigger( "reloadGrid" );
  						}
  					}, {
  						"id" : "cancel",
  						"type" : "bad",
  						"text" : "Cancel",
  						"callback" : () => {
  							hideMessageDialog("confirmArticleToGroupDelete");
  						}
  					} ]
  				});
  			} else {
  				showMessageDialog("Please select row!", {
  					"type" : "error"
  				});		
  			}
  		});
  		
  		function setGroupNameColumnProperties() {
  			var cm = $('#articleGroupsGrid').jqGrid('getColProp',"groupName");
			cm.editable = true;// Editable
			cm.edittype = "select";
			cm.formatter = "select";
			cm.editoptions = {
				aysnc: false, 
				dataUrl: "PluginServlet?action=getAddonGroups",
				buildSelect: function (data) {  
					var responseJSON = $.parseJSON(data);
					var s = "<select>";
					s += '<option value="0">--No Group--</option>';
					responseJSON.rows.map((o) => {
						console.log(o.groupId);
						s += '<option value="' + o.groupId + '">' + o.groupName + '</option>';
					});
					return s + "</select>";
				}
			}
  		}
  		
  		function setSortOrderColumnProperties() {
  			var cm = $('#articleGroupsGrid').jqGrid('getColProp',"sortOrder");
  			console.log("Sort order for select: " + sortOrderForSelect[1]);
  			
			cm.editrules = {
				number:true, 
				required:true, 
				integer: true, 
				minValue: 1,
				maxValue: (sortOrderForSelect[1] === 1) ? 1 : (sortOrderForSelect[1])  //sortOrderForSelect shows next possible value
			}
  			$("#sortOrder").val(sortOrderForSelect[1]);
			$("#sortOrder").prop("disabled", true);
  		}
  		
  		function deleteArticleToGroupOnServer(rowId) {
  			hideMessageDialog("confirmArticleToGroupDelete");
  			showInProgessDialog();
  			
  			let postData = [];
  			postData.push({	
  		    	id: rowId
  		    });

  			$.ajax({
  				type :"POST",
  			    contentType :"application/json; charset=utf-8",
  			    dataType :"json",
  			    url: "PluginServlet?action=deleteArticleToGroup",
  			    async: false,
  			    data: JSON.stringify(postData)
  			}).success((data) => {
  				hideInProgessDialog(); 	
  				showBackendCallStatus(data, "Article to Group assigment deleted successfully!"); 
  				
//  				let article = $("#articleSelectBox").val() || "";
//  				getArticleGroupsFromServer(article);
  				resetGrid();
  			}).fail((xhr, status, error) => {
  				hideInProgessDialog();
  				showMessageDialog("Error in backend call: " + xhr.responseText, {
  					"type" : "error"
  				});		
  			});
  		}
  		
  		$("#editArticleToGroup").on("click", (e) => {
  			e.preventDefault();
  			let rowid = $("#articleGroupsGrid").jqGrid("getGridParam","selrow");
  			if( rowid != null ) {
  				$("#articleGroupsGrid").jqGrid("editGridRow",rowid,editArticleFormOptions);
  			} else {
  				showMessageDialog("Please select row!", {
  					"type" : "error"
  				});
  			}
  		});
  		
  		$("#addArticleToGroup").on("click", function (e) {
  			e.preventDefault();
  			if (SELECTED_PARENT_ARTICLE && SELECTED_PARENT_ARTICLE != null && SELECTED_PARENT_ARTICLE !== "") {  				
  				$("#articleGroupsGrid").editGridRow("new", addArticleFormOptions);
  			} else {
  				showMessageDialog("Please select parent article!", {
  					"type" : "error"
  				});
  			}
  		});
  	}
     
    return {
    	createArticleGroupsGrid: createArticleGroupsGrid,
    	reloadArticleGroupsGrid: gridReload,
    	resetGrid: resetGrid
    };
}());
