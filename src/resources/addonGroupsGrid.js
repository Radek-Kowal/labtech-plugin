/*!
 * handle jQgrid AddonGroups table
 *
 */

function createAddonsGroupsGrid() {
	let groupIdForDelUpdate = "0";

	//	Prepare jQgrid
	$("#addonGroupsGrid").jqGrid({
		url : "PluginServlet?action=getAddonGroups",
		datatype : "json",
		mtype : "GET",
		ajaxGridOptions : {
			contentType : "application/json; charset=utf-8"
		},
		colNames : ["Id",
			"Name",
			"Description"],
		colModel : [{
				name : "groupId",
				index: "GroupId",
				width : 55,
				editable : false
			}, {
				name : "groupName",
				index: "GroupName",
				width : 90,
				editable : true
			}, {
				name : "groupDescription",
				index: "GroupDescription",
				width : 90,
				editable : true
			}
		],
		rowNum : 10,
		rowList : [10, 20, 30],
		autowidth : false,
		width : 600,
		rownumbers : true,
		pager : "#addonGroupsPager",
		sortname : 'id',
		viewrecords : true,
		sortorder : "asc",
		caption : "Addon Groups",
		emptyrecords : "No Groups",
		loadonce : false,
		toolbar: [true,"top"],
		jsonReader : {
			root : "rows",
			page : "page",
			total : "total",
			records : "records",
			repeatitems : false,
			cell : "cell",
			id : "groupId"
		}
	});	
	
	let jsonAddGroupOptions = {
	    type :"POST",
	    contentType :"application/json; charset=utf-8",
	    dataType :"json",
	    "url": "PluginServlet?action=addAddonGroup",
	    "async": false,
		"success": function(data){
			hideInProgessDialog(); 	
			showMessageDialog("New group added successfully!", {
				"type" : "info"
			});
		},
		"error": function(xhr, status, error){
			hideInProgessDialog();
			showMessageDialog("Error in backend call: " + + xhr.responseText, {
				"type" : "error"
			});	
		}
	};
	
	let jsonEditGroupOptions = {
	    type :"POST",
	    contentType :"application/json; charset=utf-8",
	    dataType :"json",
	    "url": "PluginServlet?action=editAddonGroup",
	    "async": false,
		"success": function(data){
			hideInProgessDialog(); 	
			showMessageDialog("Group edited successfully!", {
				"type" : "info"
			});
		},
		"error": function(xhr, status, error){
			hideInProgessDialog();
			showMessageDialog("Error in backend call: " + + xhr.responseText, {
				"type" : "error"
			});	
		}
	};

	let editFormOptions = {
		height:200,
		processData: "Processing...",
		ajaxEditOptions: jsonEditGroupOptions,
		serializeEditData: (postdata) => {
			 let output = [];
			    output.push({	
			    	groupId: groupIdForDelUpdate,
			    	groupName: $('input[name=groupName]').val(),
			    	groupDescription: $('input[name=groupDescription]').val()
			    });
			 return JSON.stringify(output);
		},
		recreateForm : true,
		closeAfterEdit : true,
		reloadAfterSubmit : true
	}
	
	let addFormOptions = {
		height:200,
		processData: "Processing...",
		ajaxEditOptions: jsonAddGroupOptions,
		serializeEditData: (postdata) => {
			 let output = [];
			    output.push({	    	
			    	groupName: $('input[name=groupName]').val(),
			    	groupDescription: $('input[name=groupDescription]').val()
			    });
			 return JSON.stringify(output);
		},
		recreateForm : true,
		closeAfterAdd : true,
		reloadAfterSubmit : true
	}
	
	$("#delete-row").click(function(){
		let rowid = jQuery("#addonGroupsGrid").jqGrid("getGridParam","selrow");
		if( rowid != null ) {
			let rowData = $("#addonGroupsGrid").jqGrid("getRowData",parseInt(rowid,10));
			groupIdForDelUpdate = rowData["groupId"];
			showMessageDialog("Delete selected record?", {
				"type" : "warning",
				"dialogId" : "confirmAddonGroupDelete",
				"buttons" : [ {
					"id" : "add",
					"type" : "good",
					"text" : "Remove",
					"callback" : () => {
						deleteAddonGroupOnServer(groupIdForDelUpdate);	
						$('#addonGroupsGrid').trigger( "reloadGrid" );
					}
				}, {
					"id" : "cancel",
					"type" : "bad",
					"text" : "Cancel",
					"callback" : () => {
						hideMessageDialog("confirmAddonGroupDelete");
					}
				} ]
			});
		} else {
			showMessageDialog("Please select row!", {
				"type" : "error"
			});		
		}
	});
	
	function deleteAddonGroupOnServer(rowId) {
		hideMessageDialog("confirmAddonGroupDelete");
		showInProgessDialog();
		
		let postData = [];
		postData.push({	
	    	groupId: rowId
	    });

		$.ajax({
			type :"POST",
		    contentType :"application/json; charset=utf-8",
		    dataType :"json",
		    url: "PluginServlet?action=deleteAddonGroup",
		    async: false,
		    data: JSON.stringify(postData)
		}).success((data) => {
			hideInProgessDialog(); 	
			showMessageDialog("Group deleted successfully!", {
				"type" : "info"
			});
		}).fail((xhr, status, error) => {
			hideInProgessDialog();
			showMessageDialog("Error in backend call: " + + xhr.responseText, {
				"type" : "error"
			});		
		});
	}
	
	$("#edit-row").click(function(){
		let rowid = jQuery("#addonGroupsGrid").jqGrid("getGridParam","selrow");
		if( rowid != null ) {
			let rowData = $("#addonGroupsGrid").jqGrid("getRowData",parseInt(rowid,10));
			groupIdForDelUpdate = rowData["groupId"];
			$("#addonGroupsGrid").jqGrid("editGridRow",rowid,editFormOptions);
		} else {
			showMessageDialog("Please select row!", {
				"type" : "error"
			});
		}
	});
	
	$("#add-new-row").on("click", function () {
		$("#addonGroupsGrid").editGridRow("new", addFormOptions);
	});
}
