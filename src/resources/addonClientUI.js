/*!
 * handle addonClientUI
 *
 */
    
let addonClientUIModule = (function() {
    'use strict';
    
    let addonGroups, addons;
    
    let dialogAddOnsClientUIHtml = `<div id="dialogAddOnsClientUI">
            <div class="table">
                <!--<div class="row" id="title">
                    <div class="cell" id="title">Addons</div>
                </div>-->
                <div class="row header">
                    <div class="cell">
                        <div id="parent">
                            <div class="parent_row_1">
                                <div class="cell" id="parent_id">Parent article id</div>
                                <div class="cell" id="parent_name">Parent article name</div>
                            </div>
                            <div class="parent_row_2">
                                <div class="cell" id="parent_id"></div>
                                <div class="cell" id="parent_name"></div>
                            </div>
                        </div>
                    </div>
                    <div class="cell">
                        <div style="float: left; margin-right: 50px;">
                            <div class="button_no_addons">No addons</div>
                        </div>
                        <div style="float: right;">
                            <div class="button_addons_add">Add addons</div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="addons_container"></div>
        </div>`;

    let addonDiv = `<div class="addon">
    					<input class="groupId" type="hidden"/>
    					<input class="itemCode" type="hidden"/>
                        <div class="addon_row">
                            <div class="addon_name"></div>
                        </div>
                        <div class="addon_row">
                            <div class="addon_cell" id="addon_quantity_title">Quantity:</div>
                            <div class="addon_cell" id="addon_quantity_value">
                                <input type="text" name="addonQty" class="text ui-widget-content ui-corner-all" style="padding-left: 3px;"/>
                            </div>
                        </div>
                        <div class="addon_row">
                            <div class="addon_cell" id="addon_price_title">Price:</div>
                            <div class="addon_cell" id="addon_price_value">
                                <input type="text" name="addonPrice" class="text ui-widget-content ui-corner-all" style="padding-left: 3px;"/>
                            </div>
                        </div>
                    </div>`;

    let groupDiv = `<div class="group">
                    	<div class="group_title"></div>                    	
                    	<div class="addons"></div>
                    	<div class="group_footer">
                    		<button class="scroll">Next group</button>
                    	</div>                        
                    </div>`;

    function handleAddonsClientUI(item){  	
    	$('body').append(dialogAddOnsClientUIHtml);
    	
    	$('.button_no_addons').hover(
	       function(){ $(this).addClass('addon_button_hover') },
	       function(){ $(this).removeClass('addon_button_hover') }
		);
    	
    	$('.button_addons_add').hover(
	       function(){ $(this).addClass('addon_button_hover') },
	       function(){ $(this).removeClass('addon_button_hover') }
		);
    	 
    	// get data
    	let cachedAddonDetails = localStorage.getItem("addonsDetails");
    	if (cachedAddonDetails) {
    		ADDONS_DETAILS = JSON.parse(cachedAddonDetails) || {};
    	} else {
    		ADDONS_DETAILS = fetchDataForAddons(item);
    	}
    	
    	// render dialog - set dialog options
    	$(".parent_row_2 #parent_id").text(ADDONS_DETAILS.itemCode);
    	$(".parent_row_2 #parent_name").text(ADDONS_DETAILS.itemName);
    	
    	// render groups with addons
    	$.each(ADDONS_DETAILS.addonGroups, (i, item) => {
    		$('.addons_container').append(groupDiv);
    		// $(groupDiv)
    		$('.addons_container :last-child').attr("data-groupId", item.id);
    		
		    $('.addons_container :last-child > .group_title').text(item.groupName);	
		    
		    $.each(item.addons, (a, addon) => {
		    	$('.addons_container :last-child > .addons').append(addonDiv);

		    	// set properties
		    	$("#dialogAddOnsClientUI > div.addons_container > div:last-child > div.addons > div:last-child > input.groupId").val(item.id);
		    	
		    	$("#dialogAddOnsClientUI > div.addons_container > div:last-child > div.addons > div:last-child > .addon_row > .addon_name").text(addon.addonDescription);
		    	
		    	// add item code to data
		    	$("#dialogAddOnsClientUI > div.addons_container > div:last-child > div.addons > div:last-child > input.itemCode").val(addon.addonArticle);
		    	$("#dialogAddOnsClientUI > div.addons_container > div:last-child > div.addons > div:last-child").attr("data-itemcode", addon.addonArticle);
		    	
		    	$("#dialogAddOnsClientUI > div.addons_container > div:last-child > div.addons > div:last-child > .addon_row div#addon_quantity_value > input").val(1)
		    	$("#dialogAddOnsClientUI > div.addons_container > div:last-child > div.addons > div:last-child > .addon_row div#addon_price_value > input").val(addon.addonPrice);
		    	// choose default addons
		    	if (addon.addonDefault === "Y") {
		    		$("#dialogAddOnsClientUI > div.addons_container > div:last-child > div.addons > div:last-child").addClass("addon_selected");
		    	}
		    });
		});
    	
    	// CHANGE SELECTION
    	$("div.addon").on("click", function(e) {
    		// toggle selection 
    		if ($(this).hasClass("addon_selected")) {
    			$(this).removeClass("addon_selected");
    		} else {    			
    			// check if single
    			let singleGroups = ADDONS_DETAILS.addonGroups.filter((g) => {return g.single === "Y";});
    			let addonsGroup = $(this).parent().find("input.groupId").val();
    			if (singleGroups.some(item => item.id == addonsGroup)) {
    				$(this).parents().children('div.addons').children('div.addon').removeClass("addon_selected");
    				$(this).addClass("addon_selected");
    			}
    		}
    	});
    	
    	// do not change for either quantity or price
    	$("input[name='addonQty']").on("click", function(e) {
    		e.stopPropagation();
    	});
    	
    	$("input[name='addonPrice']").on("click", function(e) {
    		e.stopPropagation();
    	});
    	
    	$("div.button_no_addons").on("click", (e) => {
    		$('#dialogAddOnsClientUI').dialog("close");
    		ADDONS_DETAILS = {};
//    		queryForSalesItemOrig(ORIG_ITEM);
    	});
    	
    	// ADD ITEMS TO THE RECEIPT
    	$("div.button_addons_add").on("click", (e) => {
    		ADDONS_DETAILS = {};  		
    		let addonsToAdd = [];

    		$("div.addon.addon_selected").each(function( index ) {
    			let addonItem = {
					itemCode: $( this ).children('div.addon > input.itemCode').val(),
					quantity: $(this).find("input[name='addonQty']").val(),
					price: $(this).find("input[name='addonPrice']").val()
    			}
    			addonsToAdd.push(addonItem);
			});
    		
    		$('#dialogAddOnsClientUI').dialog("close");
    		addAddonItemsToReceipt(addonsToAdd);
    	});    	
    	
    	let i18nButtons = {};
//    	i18nButtons[dialogButtonText_ok] = function() {
//    		addAddonsToReceipt();
//    	};
//    	i18nButtons[dialogButtonText_reset] = function() {
//    		$("#"+$("#dialogAddOnsClientUI").parent().attr("id") + " #dialogNotificationArea").errorStyle("Error", "");
//    	 };
    	i18nButtons[dialogButtonText_cancel] = function() { 
    		$('#dialogAddOnsClientUI').dialog("close");
    		ADDONS_DETAILS = {};
    		queryForSalesItemOrig(ORIG_ITEM);
    	};
    	
        $('#dialogAddOnsClientUI').dialog({		    	
    	  height:650,
    	  width: "80%",
    	  position: { my: "center", at: "center", of: window },
          modal: true,
          buttons:i18nButtons,
          close: function(){
        	  $("#"+$("#dialogAddOnsClientUI").parent().attr("id") + " #dialogNotificationArea").remove();
        	  $("#dialogAddOnsClientUI").remove();
          }
        });
      // Putting the Notification Area
        $("#dialogAddOnsClientUI").parent().attr("id","dialogAddOnsClientUIDialogParent");
        $("#dialogAddOnsClientUI").parent(".ui-dialog").children(".ui-dialog-buttonpane").append('<div id=\"dialogNotificationArea\"><div id=\"infoArea\"></div><div id=\"errorArea\"></div></div>');
        
        // add correct title
        $('#ui-dialog-title-dialogAddOnsClientUI').text("Choose Addons");  
    }
    
    function addAddonItemsToReceipt(postData) {
    	$.ajax({
			type :"POST",
		    contentType :"application/json; charset=utf-8",
		    dataType :"json",
		    url: "PluginServlet?action=addAddonItemsToReceipt",
		    async: false,
		    data: JSON.stringify(postData)
		}).success((data) => {
			hideInProgessDialog(); 	
			showBackendCallStatus(data, "Addon to Article assigment deleted successfully!"); 
			
		}).fail((xhr, status, error) => {
			hideInProgessDialog();
			showMessageDialog("Error in backend call: " + xhr.responseText, {
				"type" : "error"
			});		
		});
}
    
    function fetchDataForAddons(parentArticle) {
    	let data = {};
    	data["itemCode"] = parentArticle.itemCode;
    	data["itemName"] = parentArticle.itemName;
    	
    	// get addons groups from the backend
    	getAddonsGroupsFromBackend(parentArticle.itemCode);
    	
    	if (Array.isArray(addonGroups) && addonGroups.length > 0) {
    		data["addonGroups"] = addonGroups.sort((a, b)=>{return a.sortOrder - b.sortOrder});
    	}
    	
    	// get addons from the backend
    	getAddonsFromBackend(parentArticle.itemCode);
    	if (Array.isArray(addons) && addons.length > 0) {
    		$.each(data.addonGroups, (i, group) => {
    			let addonsForGroup = addons.filter((o) => { return o.articleToGroupId === group.id; }); 
    			group["addons"] = addonsForGroup.sort((a, b)=>{return a.sortOrder - b.sortOrder});
    		})
// data["addons"] = addons.sort((a, b)=>{return a.sortOrder - b.sortOrder});
    	}
    	
    	return data;
    }

    function getAddonsFromBackend(parentArticle) {
    	$.ajax({
  			type :"GET",
  		    contentType :"application/json; charset=utf-8",
  		    dataType :"json",
  		    url: "PluginServlet?action=getArticleAddonsByParent&parentArticle=" + parentArticle,
  		    async: false
  		}).success((data) => {
  			hideInProgessDialog();
  			
  			if (data["status"] === "success") {
  				addons = data.rows;
  			} else {
  				showMessageDialog("Error in backend call: " + data["message"], {
  					"type" : "error"
  				});	
  			}
  		}).fail((xhr, status, error) => {
  			hideInProgessDialog();
  			showMessageDialog("Error in backend call: " + + xhr.responseText, {
  				"type" : "error"
  			});		
  		});
    }
    
    /*
	 * TO DO: rewrite function in articleGroupsGridModule and use it here
	 */
    function getAddonsGroupsFromBackend(itemCode) {
  		$.ajax({
  			type :"GET",
  		    contentType :"application/json; charset=utf-8",
  		    dataType :"json",
  		    url: "PluginServlet?action=getArticleGroups&itemCode=" + itemCode,
  		    async: false
  		}).success((data) => {
  			hideInProgessDialog();
  			if (data["status"] === "success") {
  				addonGroups = data.rows;
  			} else {
  				showMessageDialog("Error in backend call: " + data["message"], {
  					"type" : "error"
  				});	
  			}
  		}).fail((xhr, status, error) => {
  			hideInProgessDialog();
  			showMessageDialog("Error in backend call: " + + xhr.responseText, {
  				"type" : "error"
  			});		
  		});
  	}
     
    function getArticleInfo(item) {
		showInProgressDialog();

		console.log(item);
		showInProgressDialog();
		var itemAdded = item["id"].replace(/\+/g, '%2B');

		$.ajax({
			"url" : "PluginServlet?action=getMaterialInfo&materialId=" + itemAdded,
			"dataType" : "JSON",
			"type" : "GET",
			"async": false
		}).success(function(data) {
			hideInProgressDialog();
			console.log(data);

			if (data["status"] === "success") {
				if (data && data.isParent === "Y") {
					handleAddonsClientUI(data);
				} else {
					console.log(`This is not parent article: ${itemAdded}!`);
					queryForSalesItemOrig(ORIG_ITEM);
					return;
				}
			}
			hideInProgressDialog();
		}).fail(function () {
			hideInProgessDialog();
			alert("Error!");
		});
	}
         
    return {
    	getArticleInfo: getArticleInfo
    };
}());
