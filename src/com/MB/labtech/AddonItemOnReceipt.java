package com.MB.labtech;

import java.math.BigDecimal;
import java.sql.Timestamp;

import com.sap.xi.basis.global.DateTime;

/**
 * structure for article item to be added to the receipt
 * 
 * @author RK
 */
public class AddonItemOnReceipt {
	private final String itemId;
	private Integer itemQty;
	private final BigDecimal price;

	public AddonItemOnReceipt(String itemId, BigDecimal price, Integer itemQty) {
			super();
			this.itemId = itemId;
			this.price = price;
			this.itemQty = itemQty;
		}

	public String itemId() {
		return itemId;
	}

	public BigDecimal price() {
		return price;
	}

	public Integer itemQty() {
		return itemQty;
	}

	public void setItemQty(Integer qty) {
		this.itemQty = qty;
	}

	@Override
	public String toString() {
		return String.format("%s : %s : %s", itemId, price, itemQty);
	}
}
