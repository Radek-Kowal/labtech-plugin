package com.MB.labtech;

import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
//import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.fop.apps.FOPException;
import freemarker.template.TemplateException;
import javax.xml.transform.TransformerException;
import com.sap.scco.util.exception.WrongConfigurationException;
import com.sap.scco.util.exception.WrongUsageException;
import org.apache.log4j.Logger;
import org.apache.commons.lang.StringUtils;

import com.MB.labtech.ConfigDAO;
import com.sap.scco.ap.plugin.BasePlugin;
import com.sap.scco.ap.plugin.BreakExecutionException;
import com.sap.scco.ap.plugin.annotation.ListenToExit;
import com.sap.scco.ap.plugin.annotation.ManualTriggerFunction;
import com.sap.scco.ap.plugin.annotation.PluginAt;
import com.sap.scco.ap.plugin.annotation.PluginAt.POSITION;
import com.sap.scco.ap.plugin.annotation.Schedulable;
import com.sap.scco.ap.plugin.annotation.ui.DOMInject;
import com.sap.scco.ap.plugin.annotation.ui.JSInject;
import com.sap.scco.ap.pos.dao.CDBSession;
import com.sap.scco.ap.pos.dao.CDBSessionFactory;
import com.sap.scco.ap.pos.dao.HardwareManager;
//import com.sap.scco.ap.pos.dao.EntityFactory;
import com.sap.scco.ap.pos.dao.IReceiptManager;
//import com.sap.scco.ap.pos.dao.PrintTemplateManager;
import com.sap.scco.ap.pos.dao.ReceiptManager;
import com.sap.scco.ap.pos.dto.ReceiptDTO;
import com.sap.scco.ap.pos.entity.AdditionalFieldEntity;
import com.sap.scco.ap.pos.entity.MaterialEntity;
import com.sap.scco.ap.pos.entity.PaymentItemEntity;
//import com.sap.scco.ap.pos.entity.PrintTemplateEntity;
import com.sap.scco.ap.pos.entity.ReceiptEntity;
import com.sap.scco.ap.pos.entity.SalesItemEntity;
import com.sap.scco.ap.pos.entity.UserEntity;
import com.sap.scco.ap.pos.i14y.util.context.I14YContext;
import com.sap.scco.ap.pos.job.PluginJob;
import com.sap.scco.ap.pos.printer.PrintEngine;
import com.sap.scco.ap.pos.printer.PrintJob;
import com.sap.scco.ap.pos.printer.PrintJobManager;
//import com.sap.scco.ap.pos.printer.PrintTemplateType;
import com.sap.scco.util.exception.XState;
import com.sap.scco.ap.pos.util.ModelConverter;
import com.sap.scco.ap.pos.util.TriggerParameter;
import com.sap.scco.env.UIEventDispatcher;
import com.sap.scco.util.CLogUtils;
import com.sap.scco.util.conf.CContext;
import com.sap.scco.util.types.LogGDT;
import com.sap.scco.util.types.LogItemSeverityCode;
import generated.PostInvoiceType;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class Main extends BasePlugin implements ConfigDAO {

	private static final Logger logger = Logger.getLogger(Main.class);

	private AddonGroupDAO oGroupDAO;
	private ArticleToGroupDAO oArticleToGroupDAO;
	private AddonArticleDAO oAddonArticleDAO;

	private MBRequestDelegate requestDelegate;

	@Override
	public String getId() {
		return "MB_LABTECH_PLUGIN";
	}

	@Override
	public String getName() {
		return "MB LabTech Plugin";
	}

	@Override
	public String getVersion() {
		return "1.0.3";
	}

	@Override
	public boolean persistPropertiesToDB() {
		// If this method return true, the PluginProperties will be stored in the CCO
		// Database automatically.
		// If this is false or not overwritten by the plugin class, the Plugin itself
		// has to take care about storing the properties.
		return true;
	}

	@Override
	public Map<String, String> getPluginPropertyConfig() {
		Map<String, String> propertyConfig = new HashMap<>();
		return propertyConfig;
	}

	@Override
	public void startup() {
		// This method is executed if the plugin is started. This happens during the
		// startup phase of CCO.
		super.startup();

		logger.info("startup executed");

		oGroupDAO = new AddonGroupDAO();
		oGroupDAO.setupTableForAddonGroups();

		oArticleToGroupDAO = new ArticleToGroupDAO();
		oArticleToGroupDAO.setupTableForArticleToGroup();

		oAddonArticleDAO = new AddonArticleDAO();
		oAddonArticleDAO.setupTableForAddonArticles();

		requestDelegate = new MBRequestDelegate();
	}

	@SuppressWarnings("static-access")
	@ListenToExit(exitName = "genericButtonCallback")
	public void userInputCatch(Object caller, Object[] args) {
		CDBSession session = CDBSessionFactory.instance.createSession();

		String buttonId = (String) args[0];
		String dialogId = (String) args[1];
		String input = "";
		// wa and adv
		String wa = "";
		String adv = "";

		JSONObject o = null;

		JSONArray obj = JSONArray.fromObject(args[2]);

		if (obj != null && obj.size() > 0) {
			for (int i = 0, n = obj.size(); i < n; i++) {
				o = obj.getJSONObject(i);

				if (o.isNullObject()) {
					continue;
				}
			}
		}

		// debug
		logger.info("userInputCatch; dialog: " + dialogId);
		logger.info("userInputCatch; button: " + buttonId);
		// logger.info("userInputCatch; input: " + input);

		try {
			ReceiptManager receiptMgr = new ReceiptManager(session);
			ReceiptEntity receipt = receiptMgr
					.findOrCreate((UserEntity) CContext.getInstance().get(CContext.CURRENT_USER), null, false);

			if ("layawayDocNum".equals(dialogId)) {
				if ("addLayawayNum".equals(buttonId)) {
					input = o.getString("layaway");
					logger.info("Incoming input: " + input);

					if (receipt != null) {
						receipt.addAdditionalField(new AdditionalFieldEntity("layaway", "", input));
						receiptMgr.update(receipt);
						receiptMgr.updateDB(session);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@ListenToExit(exitName = "PluginServlet.callback.get")
	public void pluginServletGet(Object caller, Object[] args) {
		// This will be called if there are any GET requests to the PluginServlet
		HttpServletRequest request = (HttpServletRequest) args[0];
		HttpServletResponse response = (HttpServletResponse) args[1];

		requestDelegate.handleRequest(request, response);
	}

	@ListenToExit(exitName = "stateChange")
	public void stateChangeExit(Object caller, Object[] args) {
		String newState = (String) args[0];
		String context = (String) args[1];

		logger.info("newState: ");
		logger.info(newState);

		if (context.endsWith("sales.jsp")) {

			if ("READY_FOR_RECEIPT".equals(newState)) {
				// call to UI to clean localStorage - after adding receipt we need to restore
				// alerts
				UIEventDispatcher.INSTANCE.dispatchAction("DYNAMIC_ACTION2", null, newState);
			}
		}
	}

	// POST HANDLING FOR DELIVERY - mostly delivery
	@ListenToExit(exitName = "PluginServlet.callback.post")
	public void pluginServletPost(Object caller, Object[] args) throws IOException {
		// This method will be called, when a HTTP call to PluginServlet is performed

		HttpServletRequest request = (HttpServletRequest) args[0];
		HttpServletResponse response = (HttpServletResponse) args[1];

		requestDelegate.handleRequest(request, response);
	}

	// INJECTION OF JS INTO 'SALES' SCREEN
	@JSInject(targetScreen = "sales")
	// It is also possible that this method returns an array of InputStream[] or a
	// String.
	public InputStream[] injectJs() {
		// This will inject some custom javascript code in the sales screen.
		ArrayList<InputStream> myJSs = new ArrayList<InputStream>();
		myJSs.add(this.getClass().getResourceAsStream("/resources/addonGroupsGrid.js"));
		myJSs.add(this.getClass().getResourceAsStream("/resources/articleGroupsGrid.js"));
		myJSs.add(this.getClass().getResourceAsStream("/resources/articleAddonsGrid.js"));
		myJSs.add(this.getClass().getResourceAsStream("/resources/custom.Styles.js"));
		myJSs.add(this.getClass().getResourceAsStream("/resources/addonClientUI.js"));
		myJSs.add(this.getClass().getResourceAsStream("/resources/gui.js"));
//		myJSs.add(this.getClass().getResourceAsStream("/resources/loadCDNDX.js"));
		myJSs.add(this.getClass().getResourceAsStream("/resources/grid.formedit.js"));
		return myJSs.toArray(new InputStream[myJSs.size()]);
	}

//	@DOMInject(targetScreen="sales")
//	//It is also possible that this method returns an array of InputStream[] or a String.
//	public InputStream injectDOMDataTable(){
//		return this.getClass().getResourceAsStream("/resources/load_css.txt");
//	}
}
