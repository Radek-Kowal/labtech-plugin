package com.MB.labtech;

/**
 * structure for addon to group assignment 
 * @author RK 
 */
public class MBArticle2GroupInfo {
	private Integer id;
	private String parentArticle;
	private Integer sortOrder;
	private Integer oldSortOrder;
	private Integer addonGroupId;
	private String addonSingle;
	private String groupName;
	
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getParentArticle() {
		return parentArticle;
	}
	public void setParentArticle(String parentArticle) {
		this.parentArticle = parentArticle;
	}
	public Integer getSortOrder() {
		return sortOrder;
	}
	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}
	public Integer getAddonGroupId() {
		return addonGroupId;
	}
	public void setAddonGroupId(Integer addonGroupId) {
		this.addonGroupId = addonGroupId;
	}
	public String getSingle() {
		return addonSingle;
	}
	public void setSingle(String addonSingle) {
		this.addonSingle = addonSingle;
	}
	public Integer getId() {
		return id;
	}	
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getOldSortOrder() {
		return oldSortOrder;
	}
	public void setOldSortOrder(Integer oldSortOrder) {
		this.oldSortOrder = oldSortOrder;
	}
}
