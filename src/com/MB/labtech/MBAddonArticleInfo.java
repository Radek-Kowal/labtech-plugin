package com.MB.labtech;

import java.math.BigDecimal;
import java.sql.Timestamp;

import com.sap.xi.basis.global.DateTime;

/**
 * structure for article group entry 
 * @author RK 
 */
public class MBAddonArticleInfo {
	private Integer id;
	private String addonArticle;
	private Integer articleToGroupId;
	private Integer sortOrder;
	private Integer oldSortOrder;
	private String addonDefault;
	private Double addonPrice;
	private String UserId;
	private Timestamp UpdateDate;
	private String addonDescription;
	
	public String getUserId() {
		return UserId;
	}
	public void setUserId(String userId) {
		UserId = userId;
	}
	public String getAddonArticle() {
		return addonArticle;
	}
	public void setAddonArticle(String addonArticle) {
		this.addonArticle = addonArticle;
	}
	public Integer getArticleToGroupId() {
		return articleToGroupId;
	}
	public void setArticleToGroupId(Integer articleToGroupId) {
		this.articleToGroupId = articleToGroupId;
	}
	public Integer getSortOrder() {
		return sortOrder;
	}
	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}
	public String getAddonDefault() {
		return addonDefault;
	}
	public void setAddonDefault(String addonDefault) {
		this.addonDefault = addonDefault;
	}
	public Double getAddonPrice() {
		return addonPrice;
	}
	public void setAddonPrice(Double addonPrice) {
		this.addonPrice = addonPrice;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Timestamp getUpdateDate() {
		return UpdateDate;
	}
	public void setUpdateDate(Timestamp updateDate) {
		UpdateDate = updateDate;
	}
	public Integer getOldSortOrder() {
		return oldSortOrder;
	}
	public void setOldSortOrder(Integer oldSortOrder) {
		this.oldSortOrder = oldSortOrder;
	}
	public String getAddonDescription() {
		return addonDescription;
	}
	public void setAddonDescription(String addonDescription) {
		this.addonDescription = addonDescription;
	}
}
