package com.MB.labtech;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.quartz.impl.matchers.StringMatcher.StringOperatorName;

import com.MB.labtech.Commons.AddonSingle;
import com.sap.scco.ap.pos.dao.CDBSession;
import com.sap.scco.ap.pos.dao.CDBSessionFactory;

/**
 * This Data Access Object will handle the DB CRUD Operations
 * 
 * @author RK
 *
 */
public class ArticleToGroupDAO {
	private static final Logger logger = Logger.getLogger(ArticleToGroupDAO.class);
	private static final String MB_ADDON_ARTICLE2GROUP_TABLE_NAME = "MB_ADDON_ARTICLE2GROUP";
	private static final String MB_ADDON_GROUPS_TABLE_NAME = "MB_ADDON_GROUPS";

	/**
	 * Create the new DB table, if not existing
	 */
	public void setupTableForArticleToGroup() {
		CDBSession session = CDBSessionFactory.instance.createSession();
		try {
			// Try to create the new table. If this fails, the table is most likely already
			// existing
			session.beginTransaction();
			EntityManager em = session.getEM();

			String sQuery = "CREATE TABLE " + MB_ADDON_ARTICLE2GROUP_TABLE_NAME + " ("
					+ "Id INTEGER NOT NULL PRIMARY KEY GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),"
					+ "ParentArticle VARCHAR(255)," + "GroupId INTEGER REFERENCES MB_ADDON_GROUPS(GroupId), "
					+ "SortOrder INTEGER, " + "AddonSingle CHAR(1) CHECK (AddonSingle IN ('Y', 'N') ), "
					+ "CONSTRAINT unique_group UNIQUE (ParentArticle, GroupId),"
					+ "CONSTRAINT unique_order UNIQUE (ParentArticle, SortOrder)"
					+ ")";

			logger.debug(sQuery);

			Query qAddonGroupsSQLTableCreate = em.createNativeQuery(sQuery);

			qAddonGroupsSQLTableCreate.executeUpdate();

			session.commitTransaction();
			logger.info("Created table for Addon2Groups.");
		} catch (PersistenceException e) {
			session.rollbackDBSession();
			String msg = e.getMessage();
			if (msg.contains("already exists")) {		
				logger.warn(String.format("%s table already existing.", MB_ADDON_ARTICLE2GROUP_TABLE_NAME));
			}
		} catch (Exception e) {
			session.rollbackDBSession();
			logger.info(e.getMessage());
			logger.info(e.getStackTrace());
		} finally {
			session.closeDBSession();
		}
	}

	public MBArticle2GroupInfo addOrUpdateAddonGroup(MBArticle2GroupInfo oArticleToGroupItem) {
		logger.info(String.format("GetAddon2GroupsItem for id: [%s]", oArticleToGroupItem.getId().toString()));

		MBArticle2GroupInfo oGroupItemFromDB = getArticleToGroupItem(oArticleToGroupItem.getId());

		if (oGroupItemFromDB != null) {
			return updateAddonGroupItem(oArticleToGroupItem);
		} else {
			return createAddonGroupItem(oArticleToGroupItem);
		}
	}

	public MBArticle2GroupInfo updateAddonGroupItem(MBArticle2GroupInfo oArticleToGroupItem) {
		CDBSession session = CDBSessionFactory.instance.createSession();
		try {
			session.beginTransaction();
			EntityManager em = session.getEM();
			Query q;
			
			Integer newSortOrder = oArticleToGroupItem.getSortOrder();
			Integer oldSortOrder = oArticleToGroupItem.getOldSortOrder();
			
			// initial update of props AND SortOrder to 0 in order to avoid unique constraint violation
			q = em.createNativeQuery("UPDATE " + MB_ADDON_ARTICLE2GROUP_TABLE_NAME
					+ " SET GroupId=?, AddonSingle=?, SortOrder=0 where Id=?");
			
			q.setParameter(1, oArticleToGroupItem.getAddonGroupId());
			q.setParameter(2, oArticleToGroupItem.getSingle());
			q.setParameter(3, oArticleToGroupItem.getId());
			q.executeUpdate();
			
			if (newSortOrder < oldSortOrder) {
				q = em.createNativeQuery("UPDATE " + MB_ADDON_ARTICLE2GROUP_TABLE_NAME
						+ " SET SortOrder = SortOrder + 1 WHERE SortOrder >= ? AND SortOrder < ? AND ParentArticle=?");
				q.setParameter(1, newSortOrder);
				q.setParameter(2, oldSortOrder);
				q.setParameter(3, oArticleToGroupItem.getParentArticle());
				q.executeUpdate();
			} else if (newSortOrder > oldSortOrder) {
				q = em.createNativeQuery("UPDATE " + MB_ADDON_ARTICLE2GROUP_TABLE_NAME
						+ " SET SortOrder = SortOrder - 1 WHERE SortOrder > ? AND SortOrder <= ? AND ParentArticle=?");
				q.setParameter(1, oldSortOrder);
				q.setParameter(2, newSortOrder);
				q.setParameter(3, oArticleToGroupItem.getParentArticle());
				q.executeUpdate();
			}		
			
			// final update to set the correct sort order
			q = em.createNativeQuery("UPDATE " + MB_ADDON_ARTICLE2GROUP_TABLE_NAME
					+ " SET SortOrder=? where Id=?");
			
			q.setParameter(1, newSortOrder);
			q.setParameter(2, oArticleToGroupItem.getId());

			q.executeUpdate();

			session.commitTransaction();
		} catch (PersistenceException e) {
			session.rollbackDBSession();
			String msg = e.getMessage();
			if (msg.contains("UNIQUE_ORDER")) {			
				throw new RuntimeException("You cannot enter the same 'SORT ORDER' for the second time!");
			} else if (msg.contains("UNIQUE_GROUP")) {
				throw new RuntimeException("You cannot enter the same 'ADDON GROUP' for the second time!");
			}
		} catch (RuntimeException e) {
			session.rollbackDBSession();
			throw e;
		} finally {
			session.closeDBSession();
		}

		return oArticleToGroupItem;
	}

	public MBArticle2GroupInfo createAddonGroupItem(MBArticle2GroupInfo oArticleToGroupItem) {
		CDBSession session = CDBSessionFactory.instance.createSession();
		try {
			session.beginTransaction();
			EntityManager em = session.getEM();
			Query q = em.createNativeQuery("INSERT INTO " + MB_ADDON_ARTICLE2GROUP_TABLE_NAME
					+ " (ParentArticle, GroupId, SortOrder, AddonSingle) VALUES(?,?,?,?)");

			q.setParameter(1, oArticleToGroupItem.getParentArticle());
			q.setParameter(2, oArticleToGroupItem.getAddonGroupId());
			q.setParameter(3, oArticleToGroupItem.getSortOrder());
			q.setParameter(4, oArticleToGroupItem.getSingle());
			
			logger.info(String.format("addingArticleToGroup. parent: [%s], group: [%s]", oArticleToGroupItem.getParentArticle(), oArticleToGroupItem.getAddonGroupId()));

			q.executeUpdate();
			session.commitTransaction();
		} catch (PersistenceException e) {
			session.rollbackDBSession();
			String msg = e.getMessage();
			if (msg.contains("UNIQUE_ORDER")) {			
				throw new RuntimeException("You cannot enter the same 'SORT ORDER' for the second time!");
			} else if (msg.contains("UNIQUE_GROUP")) {
				throw new RuntimeException("You cannot enter the same 'ADDON GROUP' for the second time!");
			}
		} catch (Exception e) {
			session.rollbackDBSession();
			throw e;
		} finally {
			session.closeDBSession();
		}

		return oArticleToGroupItem;
	}
	
	public MBArticle2GroupInfo deleteAddonGroupItem(MBArticle2GroupInfo oArticleToGroupItem) {
		CDBSession session = CDBSessionFactory.instance.createSession();
		try {
			session.beginTransaction();
			EntityManager em = session.getEM();
			Query q = em.createNativeQuery(
					"DELETE FROM " + MB_ADDON_ARTICLE2GROUP_TABLE_NAME + " WHERE Id=?");

			q.setParameter(1, oArticleToGroupItem.getId());

			q.executeUpdate();
			session.commitTransaction();
		} catch (RuntimeException e) {
			session.rollbackDBSession();
			throw e;
		} finally {
			session.closeDBSession();
		}

		return oArticleToGroupItem;
	}

	public MBArticle2GroupInfo getArticleToGroupItem(Integer id) {
		CDBSession session = CDBSessionFactory.instance.createSession();
		try {
			EntityManager em = session.getEM();
			Query q = em.createNativeQuery("SELECT * FROM " + MB_ADDON_ARTICLE2GROUP_TABLE_NAME + " where Id=?");
			q.setParameter(1, id);

			logger.info("getArticleToGroupItem : " + q.toString());

			@SuppressWarnings("unchecked")
			List<Object[]> results = q.getResultList();

			if (results.isEmpty()) {
				return null;
			}

			return mapResultRow(results.get(0));
		} finally {
			session.closeDBSession();
		}
	}
	
	public MBArticle2GroupInfo getArticleToGroupItemBySortOrder(Integer sortOrderId) {
		CDBSession session = CDBSessionFactory.instance.createSession();
		try {
			EntityManager em = session.getEM();
			Query q = em.createNativeQuery("SELECT * FROM " + MB_ADDON_ARTICLE2GROUP_TABLE_NAME + " where SortOrder=?");
			q.setParameter(1, sortOrderId);

			logger.info("getArticleToGroupItem : " + q.toString());

			@SuppressWarnings("unchecked")
			List<Object[]> results = q.getResultList();

			if (results.isEmpty()) {
				return null;
			}

			return mapResultRow(results.get(0));
		} finally {
			session.closeDBSession();
		}
	}

	public List<MBArticle2GroupInfo> getArticleToGroupItems(String sParentArticle) {
		CDBSession session = CDBSessionFactory.instance.createSession();
		List<MBArticle2GroupInfo> resultList = new ArrayList<>();

		try {
			EntityManager em = session.getEM();
			// select * from MB_ADDON_ARTICLE2GROUP MA JOIN MB_ADDON_GROUPS MG ON MA.GroupId
			// = MG.GroupId
			Query q = em.createNativeQuery(
					"SELECT  MA.Id, MA.ParentArticle, MA.GroupId, MA.SortOrder, MA.AddonSingle, MG.GroupName FROM "
							+ MB_ADDON_ARTICLE2GROUP_TABLE_NAME + " MA JOIN " + MB_ADDON_GROUPS_TABLE_NAME
							+ " MG ON MA.GroupId = MG.GroupId WHERE MA.ParentArticle=?");
			q.setParameter(1, sParentArticle);

			logger.info("getArticleToGroupItems : " + q.toString());

			@SuppressWarnings("unchecked")
			List<Object[]> results = q.getResultList();
			for (Object[] result : results) {
				resultList.add(mapResultRow(result));
			}
		} catch (ClassCastException e) {
			logger.info(e.getMessage());
		} catch (Exception e) {
			logger.warn(e.getMessage());
			setupTableForArticleToGroup();
		} finally {
			session.closeDBSession();
		}

		return resultList;
	}

	public List<MBArticle2GroupInfo> getArticleToGroupItems() {
		List<MBArticle2GroupInfo> resultList = new ArrayList<>();

		CDBSession session = CDBSessionFactory.instance.createSession();
		try {
			EntityManager em = session.getEM();
			Query q = em.createNativeQuery("SELECT * FROM " + MB_ADDON_ARTICLE2GROUP_TABLE_NAME);

			logger.info("getArticleToGroupItems : " + q.toString());

			@SuppressWarnings("unchecked")
			List<Object[]> results = q.getResultList();
			for (Object[] result : results) {
				resultList.add(mapResultRow(result));
			}
		} catch (ClassCastException e) {
			logger.info(e.getMessage());
		} catch (Exception e) {
			logger.warn(e.getMessage());
			setupTableForArticleToGroup();
		} finally {
			session.closeDBSession();
		}

		return resultList;
	}

	public void dropAll() {
		CDBSession session = CDBSessionFactory.instance.createSession();
		try {
			session.beginTransaction();
			EntityManager em = session.getEM();
			// Query q = em.createNativeQuery("DELETE FROM " + MB_TOWNS_TABLE_NAME );
			Query q = em.createNativeQuery("DROP TABLE " + MB_ADDON_ARTICLE2GROUP_TABLE_NAME);
			q.executeUpdate();
			session.commitTransaction();
		} catch (RuntimeException e) {
			session.rollbackDBSession();
			throw e;
		} finally {
			session.closeDBSession();
		}
	}

	private MBArticle2GroupInfo mapResultRow(Object[] result) {
		MBArticle2GroupInfo oArticleToGroupItem = new MBArticle2GroupInfo();

		oArticleToGroupItem.setId(Integer.parseInt(result[0].toString()));
		oArticleToGroupItem.setParentArticle(result[1].toString());
		oArticleToGroupItem.setAddonGroupId((Integer) result[2]);
		oArticleToGroupItem.setSortOrder((Integer) result[3]);
		oArticleToGroupItem.setSingle(result[4].toString());
		if (result[5] != null) {
			oArticleToGroupItem.setGroupName(result[5].toString());
		}

		return oArticleToGroupItem;
	}
}
