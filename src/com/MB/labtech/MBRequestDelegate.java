package com.MB.labtech;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Predicate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.MB.labtech.Commons.AddonSingle;
import com.sap.scco.ap.pos.dao.CDBSession;
import com.sap.scco.ap.pos.dao.CDBSessionFactory;
import com.sap.scco.ap.pos.dao.IPriceDiscountManager;
import com.sap.scco.ap.pos.dao.MaterialManager;
import com.sap.scco.ap.pos.dao.PriceDiscountManager;
import com.sap.scco.ap.pos.dao.PriceListManager;
import com.sap.scco.ap.pos.dao.ReceiptManager;
import com.sap.scco.ap.pos.entity.AdditionalFieldEntity;
import com.sap.scco.ap.pos.entity.BusinessPartnerEntity;
import com.sap.scco.ap.pos.entity.MaterialEntity;
import com.sap.scco.ap.pos.entity.PermissionEntity;
import com.sap.scco.ap.pos.entity.PriceDiscountEntity;
import com.sap.scco.ap.pos.entity.PriceElementEntity;
import com.sap.scco.ap.pos.entity.PriceListEntity;
import com.sap.scco.ap.pos.entity.ReceiptEntity;
import com.sap.scco.ap.pos.entity.SalesItemEntity;
import com.sap.scco.ap.pos.entity.UserEntity;
import com.sap.scco.env.UIEventDispatcher;
import com.sap.scco.util.conf.CContext;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * This class handles the requests to the Plugin Servlet
 * 
 * @author RK - based on example by D060523
 *
 */
public class MBRequestDelegate {
	private static final Logger logger = Logger.getLogger(MBRequestDelegate.class);

	private ConfigDAO configDAO;

	public void setConfigDAO(ConfigDAO configDAO) {
		this.configDAO = configDAO;
	}

	public MBRequestDelegate() {

	}

	/**
	 * This method will dispatch the requests
	 * 
	 * @param request
	 * @param response
	 */
	public void handleRequest(HttpServletRequest request, HttpServletResponse response) {

		JSONObject oResponseObj = null;
		String oRequestInput;
		try {
			// Read the json data
			oRequestInput = request.getReader().readLine();
			JSONArray oRequestInputJSON = JSONArray.fromObject(oRequestInput);
			System.out.println(oRequestInputJSON);

			// Get the action query parameter from the request
			String requestAction = request.getParameter("action");
//			Map<String, Object> pars = new HashMap<>();

			// Switch for requested actions
			switch (requestAction) {
			case "getArticles":
				// Get Articles combo values
				oResponseObj = getArticlesComboVals();
				break;
			case "getAddonGroups":
				// Get AddonGroups grid values
				oResponseObj = getAddonGroups();
				break;
			case "addAddonGroup":
				// Get AddonGroups grid values
				oResponseObj = addAddonGroups(oRequestInputJSON);
				break;
			case "editAddonGroup":
				// Get AddonGroups grid values
				oResponseObj = editAddonGroups(oRequestInputJSON);
				break;
			case "deleteAddonGroup":
				// Get AddonGroups grid values
				oResponseObj = deleteAddonGroups(oRequestInputJSON);
				break;
			case "getArticleGroups":
				// Get Article Info
				String itemCode = request.getParameter("itemCode");
				// Get AddonGroups grid values
				oResponseObj = getArticleGroups(itemCode);
				break;
			case "addArticleToGroup":
				oResponseObj = addArticleAddonGroup(oRequestInputJSON);
				break;
			case "editArticleToGroup":
				oResponseObj = editArticleAddonGroup(oRequestInputJSON);
				break;
			case "deleteArticleToGroup":
				oResponseObj = deleteArticleAddonGroup(oRequestInputJSON);
				break;
			case "getArticleAddons":
				String addonArticleGroupId = request.getParameter("addonArticleGroupId");
				oResponseObj = getArticleAddons(addonArticleGroupId);
				break;
			case "getArticleAddonsByParent":
				String sAddonParentCode = request.getParameter("parentArticle");
				oResponseObj = getArticleAddonsByParent(sAddonParentCode);
				break;
			case "addArticleAddon":
				oResponseObj = addArticleAddon(oRequestInputJSON);
				break;
			case "editArticleAddon":
				oResponseObj = editArticleAddon(oRequestInputJSON);
				break;
			case "deleteArticleAddon":
				oResponseObj = deleteArticleAddon(oRequestInputJSON);
				break;
			case "getArticleAddonsUI":
				// Get Article Info
				String item = request.getParameter("itemCode");
				// Get AddonGroups grid values
				oResponseObj = getArticleGroups(item);
				break;
			case "getMaterialInfo":
				// Get Material Info
				String materialId = request.getParameter("materialId");
				oResponseObj = getMaterialInfo(materialId);
				break;
			case "getCurrentUserInfo":
				oResponseObj = getCurrentUserInfo();
				break;
			case "addAddonItemsToReceipt":
				oResponseObj = addAddonItemsToReceipt(oRequestInputJSON);
				break;
			default:
				oResponseObj = new JSONObject();
			}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			oResponseObj = new JSONObject();
			oResponseObj.put("status", "error");
			oResponseObj.put("message", e1.getMessage());
		}

		try (OutputStreamWriter osw = new OutputStreamWriter(response.getOutputStream())) {
			// Write the answer to the client
			// logger.info(responseObj.toString());

			osw.write(oResponseObj.toString());
		} catch (IOException e) {
			logger.error("Error while processing request", e);
		}
	}

	/**
	 * Get information about a material
	 * 
	 * @param materialId
	 * @return
	 */
	private JSONObject getMaterialInfo(String materialId) {
		CDBSession session = CDBSessionFactory.instance.createSession();
		JSONObject response = new JSONObject();

		try {
			MaterialManager matMgr = new MaterialManager(session);
			MaterialEntity material = matMgr.readMaterialByExternalID(materialId);

			if (material != null) {
				response.put("status", "success");

				if (material.getUdfBoolean1()) {
					response.put("isParent", "Y");
					response.put("itemName", material.getDescription());
					response.put("itemCode", materialId);
				} else {
					response.put("isParent", "N");
				}
			}
		} catch (Exception e) {
			response.put("status", "error");
			response.put("message", e.getMessage());
		} finally {
			session.closeDBSession();
		}
		return response;
	}

	/**
	 * Adds addArticleToGroup
	 * 
	 * @param
	 * @return
	 */
	private JSONObject addArticleAddonGroup(JSONArray oRequestInput) {
		logger.info("Add article 2 group");

		JSONObject response = new JSONObject();
		CDBSession session = CDBSessionFactory.instance.createSession();
		List<MBArticle2GroupInfo> oArticleGroups = new ArrayList<MBArticle2GroupInfo>();

		try {
			if (oRequestInput != null && oRequestInput.size() > 0) {
				for (int i = 0, n = oRequestInput.size(); i < n; i++) {
					JSONObject o = oRequestInput.getJSONObject(i);

					if (o.isNullObject()) {
						continue;
					}

					MBArticle2GroupInfo oArticle2GroupInfo = new MBArticle2GroupInfo();
					ArticleToGroupDAO oArticleToGroupDAO = new ArticleToGroupDAO();

					String sParentArticle = o.getString("parentArticle");

					oArticle2GroupInfo.setParentArticle(sParentArticle);

					oArticle2GroupInfo.setAddonGroupId(o.getInt("groupId"));
					oArticle2GroupInfo.setSingle(o.getString("addonSingle"));
					oArticle2GroupInfo.setSortOrder(o.getInt("sortOrder"));

					oArticleToGroupDAO.createAddonGroupItem(oArticle2GroupInfo);

					// Get items from the db
					oArticleGroups = oArticleToGroupDAO.getArticleToGroupItems(sParentArticle);

					response.put("status", "success");
					response.put("total", "1");
					response.put("page", "1");
					response.put("records", oArticleGroups.size());
					response.put("rows", oArticleGroups);
				}
			} else {
				response.put("status", "error");
				response.put("message", "Empty input!");
			}

		} catch (Exception e) {
			response.put("status", "error");
			response.put("message", e.getMessage());
		} finally {
			session.closeDBSession();
		}

		logger.info("returning value from adding addongroup");
		logger.info(response);
		return response;
	}

	/**
	 * editArticleToGroup
	 * 
	 * @param
	 * @return
	 */
	private JSONObject editArticleAddonGroup(JSONArray oRequestInput) {
		logger.info("Edit articleTogroup");

		JSONObject response = new JSONObject();
		CDBSession session = CDBSessionFactory.instance.createSession();
		List<MBArticle2GroupInfo> oArticleGroups = new ArrayList<MBArticle2GroupInfo>();

		try {
			if (oRequestInput != null && oRequestInput.size() > 0) {
				for (int i = 0, n = oRequestInput.size(); i < n; i++) {
					JSONObject o = oRequestInput.getJSONObject(i);

					if (o.isNullObject()) {
						continue;
					}

					MBArticle2GroupInfo oArticle2GroupInfo = new MBArticle2GroupInfo();
					ArticleToGroupDAO oArticleToGroupDAO = new ArticleToGroupDAO();

					String sParentArticle = o.getString("parentArticle");

					oArticle2GroupInfo.setParentArticle(sParentArticle);
					oArticle2GroupInfo.setId(o.getInt("id"));
					oArticle2GroupInfo.setAddonGroupId(o.getInt("groupId"));
					oArticle2GroupInfo.setSingle(o.getString("addonSingle"));

					oArticle2GroupInfo.setSortOrder(o.getInt("sortOrder"));
					oArticle2GroupInfo.setOldSortOrder(o.getInt("oldSortOrder"));

					oArticleToGroupDAO.updateAddonGroupItem(oArticle2GroupInfo);

					// Get items from the db
					oArticleGroups = oArticleToGroupDAO.getArticleToGroupItems(sParentArticle);

					response.put("status", "success");
					response.put("total", "1");
					response.put("page", "1");
					response.put("records", oArticleGroups.size());
					response.put("rows", oArticleGroups);
				}
			} else {
				response.put("status", "error");
				response.put("message", "Empty input!");
			}

		} catch (Exception e) {
			response.put("status", "error");
			response.put("message", e.getMessage());
		} finally {
			session.closeDBSession();
		}

		logger.info("returning value from editing addongroup");
		logger.info(response);
		return response;
	}

	/**
	 * delete ArticleToGroup
	 * 
	 * @param
	 * @return
	 */
	private JSONObject deleteArticleAddonGroup(JSONArray oRequestInput) {
		logger.info("Getting articles");

		JSONObject response = new JSONObject();
		CDBSession session = CDBSessionFactory.instance.createSession();

		try {
			if (oRequestInput != null && oRequestInput.size() > 0) {
				for (int i = 0, n = oRequestInput.size(); i < n; i++) {
					JSONObject o = oRequestInput.getJSONObject(i);

					if (o.isNullObject()) {
						continue;
					}

					MBArticle2GroupInfo oArticle2GroupInfo = new MBArticle2GroupInfo();
					ArticleToGroupDAO oArticleToGroupDAO = new ArticleToGroupDAO();

					oArticle2GroupInfo.setId(o.getInt("id"));

					oArticleToGroupDAO.deleteAddonGroupItem(oArticle2GroupInfo);

					response.put("status", "success");
				}
			} else {
				response.put("status", "error");
				response.put("message", "Empty input!");
			}

		} catch (Exception e) {
			response.put("status", "error");
			response.put("message", e.getMessage());
		} finally {
			session.closeDBSession();
		}

		logger.info("returning value from editing addongroup");
		logger.info(response);
		return response;
	}

	/**
	 * Adds addongroups
	 * 
	 * @param
	 * @return
	 */
	private JSONObject addAddonGroups(JSONArray oRequestInput) {
		logger.info("Getting articles");

		JSONObject response = new JSONObject();
		CDBSession session = CDBSessionFactory.instance.createSession();

		try {
			if (oRequestInput != null && oRequestInput.size() > 0) {
				for (int i = 0, n = oRequestInput.size(); i < n; i++) {
					JSONObject o = oRequestInput.getJSONObject(i);

					if (o.isNullObject()) {
						continue;
					}

					MBArticleGroupInfo oAddonGroupInfo = new MBArticleGroupInfo();
					AddonGroupDAO oAddonGroupDAO = new AddonGroupDAO();

					oAddonGroupInfo.setGroupName(o.getString("groupName"));
					oAddonGroupInfo.setGroupDescription(o.getString("groupDescription"));

					oAddonGroupDAO.addOrUpdateAddonGroup(oAddonGroupInfo);

					response.put("status", "success");
				}
			} else {
				response.put("status", "error");
				response.put("message", "Empty input!");
			}

		} catch (Exception e) {
			response.put("status", "error");
			response.put("message", e.getMessage());
		} finally {
			session.closeDBSession();
		}

		logger.info("returning value from adding addongroup");
		logger.info(response);
		return response;
	}

	/**
	 * edit addongroup
	 * 
	 * @param
	 * @return
	 */
	private JSONObject editAddonGroups(JSONArray oRequestInput) {
		logger.info("Getting articles");

		JSONObject response = new JSONObject();
		CDBSession session = CDBSessionFactory.instance.createSession();

		try {
			if (oRequestInput != null && oRequestInput.size() > 0) {
				for (int i = 0, n = oRequestInput.size(); i < n; i++) {
					JSONObject o = oRequestInput.getJSONObject(i);

					if (o.isNullObject()) {
						continue;
					}

					MBArticleGroupInfo oAddonGroupInfo = new MBArticleGroupInfo();
					AddonGroupDAO oAddonGroupDAO = new AddonGroupDAO();

					oAddonGroupInfo.setGroupId(o.getInt("groupId"));
					oAddonGroupInfo.setGroupName(o.getString("groupName"));
					oAddonGroupInfo.setGroupDescription(o.getString("groupDescription"));

					oAddonGroupDAO.addOrUpdateAddonGroup(oAddonGroupInfo);

					response.put("status", "success");
				}
			} else {
				response.put("status", "error");
				response.put("message", "Empty input!");
			}

		} catch (Exception e) {
			response.put("status", "error");
			response.put("message", e.getMessage());
		} finally {
			session.closeDBSession();
		}

		logger.info("returning value from editing addongroup");
		logger.info(response);
		return response;
	}

	/**
	 * delete addongroup
	 * 
	 * @param
	 * @return
	 */
	private JSONObject deleteAddonGroups(JSONArray oRequestInput) {
		logger.info("Getting articles");

		JSONObject response = new JSONObject();
		CDBSession session = CDBSessionFactory.instance.createSession();

		try {
			if (oRequestInput != null && oRequestInput.size() > 0) {
				for (int i = 0, n = oRequestInput.size(); i < n; i++) {
					JSONObject o = oRequestInput.getJSONObject(i);

					if (o.isNullObject()) {
						continue;
					}

					MBArticleGroupInfo oAddonGroupInfo = new MBArticleGroupInfo();
					AddonGroupDAO oAddonGroupDAO = new AddonGroupDAO();

					oAddonGroupInfo.setGroupId(o.getInt("groupId"));

					oAddonGroupDAO.deleteAddonGroupItem(oAddonGroupInfo);

					response.put("status", "success");
				}
			} else {
				response.put("status", "error");
				response.put("message", "Empty input!");
			}

		} catch (Exception e) {
			response.put("status", "error");
			response.put("message", e.getMessage());
		} finally {
			session.closeDBSession();
		}

		logger.info("returning value from editing addongroup");
		logger.info(response);
		return response;
	}

	/**
	 * Read articles for combo
	 * 
	 * @param
	 * @return
	 */
	private JSONObject getArticlesComboVals() {
		logger.info("Getting articles");

		JSONObject response = new JSONObject();
		CDBSession session = CDBSessionFactory.instance.createSession();

		MaterialManager materialManager = new MaterialManager(session);
		PriceListManager priceListManager = new PriceListManager(session);
		IPriceDiscountManager priceDiscountManager = new PriceDiscountManager(session);

		// create objects
		List<MBArticleInfo> MBItems = new ArrayList<MBArticleInfo>();
		MBArticleInfo MBItem = null;

		try {
			// Get items from the db
			List<MaterialEntity> Articles = materialManager.getAll();

			// Get price list
			PriceListEntity priceList = priceListManager.getDefaultPriceList();

			List<PriceDiscountEntity> listOfPriceDiscounts = priceList.getPriceDiscounts();

			// Build the response object
			logger.info("Articles object built");

			// prepare JSON Array to return
			for (MaterialEntity item : Articles) {
				// create margin object
				MBItem = new MBArticleInfo();
				MBItem.setItemCode(item.getExternalID());
				MBItem.setItemName(item.getDescription());

//				PriceDiscountEntity priceDiscount = item.getPriceDiscount();
				PriceDiscountEntity priceDiscount = priceDiscountManager.readPriceDiscountByMaterial(item);

//				BigDecimal price = priceDiscount.getDiscount();
				BigDecimal price = priceDiscount.getGrossPrice();

				MBItem.setUnitPrice(price);

				MBItem.setIsParent(item.getUdfBoolean1());

				// add it to list
				MBItems.add(MBItem);
			}

			response.put("status", "success");
			response.put("total", "1");
			response.put("page", "1");
			response.put("records", MBItems.size());
			response.put("rows", MBItems);

			logger.info("Articles combo get success set");

			response.put("status", "success");
			return response;
		} catch (Exception e) {
			logger.error("Error while retrieving articles", e);
			e.printStackTrace();

			response.put("status", "error");
			response.put("message", "Error in communication with backend system: " + e.getMessage());

			return response;
		} finally {
			session.closeDBSession();
		}
	}

	/**
	 * Read addongroups for grid
	 * 
	 * @param
	 * @return
	 */
	private JSONObject getAddonGroups() {
		logger.info("Getting addon groups");

		JSONObject response = new JSONObject();
		CDBSession session = CDBSessionFactory.instance.createSession();

		// create objects
		AddonGroupDAO oGroupDAO = new AddonGroupDAO();
		List<MBArticleGroupInfo> oAddonGroups = new ArrayList<MBArticleGroupInfo>();

		try {
			// Get items from the db
			oAddonGroups = oGroupDAO.getAddonGroupItems();

			response.put("status", "success");
			response.put("total", "1");
			response.put("page", "1");
			response.put("records", oAddonGroups.size());
			response.put("rows", oAddonGroups);

			logger.info("Addon Groups get success set");

			response.put("status", "success");
			return response;
		} catch (Exception e) {
			logger.error("Error while retrieving addon groups", e);
			e.printStackTrace();

			response.put("status", "error");
			response.put("message", "Error in communication with backend system: " + e.getMessage());

			return response;
		} finally {
			session.closeDBSession();
		}
	}

	/**
	 * Read articlegroups for grid
	 * 
	 * @param
	 * @return
	 */
	private JSONObject getArticleGroups(String sParentArticle) {
		logger.info("Getting article groups");

		JSONObject response = new JSONObject();
		CDBSession session = CDBSessionFactory.instance.createSession();

		// create objects
		ArticleToGroupDAO oArticleGroupDAO = new ArticleToGroupDAO();
		List<MBArticle2GroupInfo> oArticleGroups = new ArrayList<MBArticle2GroupInfo>();

		try {
			if (!"0".equals(sParentArticle) && sParentArticle != null && !sParentArticle.isEmpty()) {
				// Get items from the db
				oArticleGroups = oArticleGroupDAO.getArticleToGroupItems(sParentArticle);

				response.put("status", "success");
				response.put("total", "1");
				response.put("page", "1");
				response.put("records", oArticleGroups.size());
				response.put("rows", oArticleGroups);

				logger.info("Addon Groups get success set");

				response.put("status", "success");
			} else {
				response.put("status", "error");
				response.put("message", "Parent article not found!");
			}
			return response;
		} catch (Exception e) {
			response.put("status", "error");
			response.put("message", "Error in communication with backend system: " + e.getMessage());

			return response;
		} finally {
			session.closeDBSession();
		}
	}

	/**
	 * Read articleaddons by parent for grid
	 * 
	 * @param
	 * @return
	 */
	private JSONObject getArticleAddonsByParent(String sAddonParentCode) {
		logger.info("Getting article addons");

		JSONObject response = new JSONObject();
		CDBSession session = CDBSessionFactory.instance.createSession();

		// create objects
		AddonArticleDAO oAddonArticleDAO = new AddonArticleDAO();
		List<MBAddonArticleInfo> oArticleAddons = new ArrayList<MBAddonArticleInfo>();

		try {
			if (!StringUtils.isEmpty(sAddonParentCode)) {
				// Get items from the db
				oArticleAddons = oAddonArticleDAO.getAddonArticleItems(sAddonParentCode);

				response.put("status", "success");
				response.put("total", "1");
				response.put("page", "1");
				response.put("records", oArticleAddons.size());
				response.put("rows", oArticleAddons);

				logger.info("Addon Articles get success set");

				response.put("status", "success");
			} else {
				response.put("status", "error");
				response.put("message", "Addons articles not found!");
			}
			return response;
		} catch (Exception e) {
			response.put("status", "error");
			response.put("message", "Error in communication with backend system: " + e.getMessage());

			return response;
		} finally {
			session.closeDBSession();
		}
	}

	/**
	 * Read articleaddons for grid
	 * 
	 * @param
	 * @return
	 */
	private JSONObject getArticleAddons(String sAddonArticleGroupId) {
		logger.info("Getting article addons");

		JSONObject response = new JSONObject();
		CDBSession session = CDBSessionFactory.instance.createSession();

		// create objects
		AddonArticleDAO oAddonArticleDAO = new AddonArticleDAO();
		List<MBAddonArticleInfo> oArticleAddons = new ArrayList<MBAddonArticleInfo>();

		try {
			if (!"0".equals(sAddonArticleGroupId) && sAddonArticleGroupId != null && !sAddonArticleGroupId.isEmpty()) {
				// Get items from the db
				oArticleAddons = oAddonArticleDAO.getAddonArticleItems(Integer.parseInt(sAddonArticleGroupId));

				response.put("status", "success");
				response.put("total", "1");
				response.put("page", "1");
				response.put("records", oArticleAddons.size());
				response.put("rows", oArticleAddons);

				logger.info("Addon Articles get success set");

				response.put("status", "success");
			} else {
				response.put("status", "error");
				response.put("message", "Addons articles not found!");
			}
			return response;
		} catch (Exception e) {
			response.put("status", "error");
			response.put("message", "Error in communication with backend system: " + e.getMessage());

			return response;
		} finally {
			session.closeDBSession();
		}
	}

	/**
	 * Adds ArticleAddon
	 * 
	 * @param
	 * @return
	 */
	private JSONObject addArticleAddon(JSONArray oRequestInput) {
		logger.info("Add article 2 group");

		JSONObject response = new JSONObject();
		CDBSession session = CDBSessionFactory.instance.createSession();
		List<MBAddonArticleInfo> oArticleAddons = new ArrayList<MBAddonArticleInfo>();

		try {
			if (oRequestInput != null && oRequestInput.size() > 0) {
				for (int i = 0, n = oRequestInput.size(); i < n; i++) {
					JSONObject o = oRequestInput.getJSONObject(i);

					if (o.isNullObject()) {
						continue;
					}

					MBAddonArticleInfo oAddonArticleInfo = new MBAddonArticleInfo();
					AddonArticleDAO oAddonArticleDAO = new AddonArticleDAO();

					String sAddonArticle = o.getString("addonArticle");
					Integer iArticleToGroupId = o.getInt("articleToGroupId");

					oAddonArticleInfo.setAddonArticle(sAddonArticle);

					oAddonArticleInfo.setArticleToGroupId(iArticleToGroupId);
					oAddonArticleInfo.setAddonDefault(o.getString("addonDefault"));
					oAddonArticleInfo.setSortOrder(o.getInt("sortOrder"));
					oAddonArticleInfo.setAddonPrice(o.getDouble("addonPrice"));

					oAddonArticleDAO.createAddonArticleItem(oAddonArticleInfo);

					// Get items from the db
					oArticleAddons = oAddonArticleDAO.getAddonArticleItems(iArticleToGroupId);

					response.put("status", "success");
					response.put("total", "1");
					response.put("page", "1");
					response.put("records", oArticleAddons.size());
					response.put("rows", oArticleAddons);
				}
			} else {
				response.put("status", "error");
				response.put("message", "Empty input!");
			}

		} catch (Exception e) {
			response.put("status", "error");
			response.put("message", e.getMessage());
		} finally {
			session.closeDBSession();
		}

		logger.info("returning value from adding article addon");
		logger.info(response);
		return response;
	}

	/**
	 * edit AddonToArticle
	 * 
	 * @param
	 * @return
	 */
	private JSONObject editArticleAddon(JSONArray oRequestInput) {
		logger.info("Edit addon to article");

		JSONObject response = new JSONObject();
		CDBSession session = CDBSessionFactory.instance.createSession();
		List<MBAddonArticleInfo> oArticleAddons = new ArrayList<MBAddonArticleInfo>();

		try {
			if (oRequestInput != null && oRequestInput.size() > 0) {
				for (int i = 0, n = oRequestInput.size(); i < n; i++) {
					JSONObject o = oRequestInput.getJSONObject(i);

					if (o.isNullObject()) {
						continue;
					}

					MBAddonArticleInfo oAddonArticleInfo = new MBAddonArticleInfo();
					AddonArticleDAO oAddonArticleDAO = new AddonArticleDAO();

					String sAddonArticle = o.getString("addonArticle");
					Integer iArticleToGroupId = o.getInt("articleToGroupId");

					oAddonArticleInfo.setAddonArticle(sAddonArticle);

					oAddonArticleInfo.setArticleToGroupId(iArticleToGroupId);
					oAddonArticleInfo.setAddonDefault(o.getString("addonDefault"));
					oAddonArticleInfo.setId(o.getInt("id"));
					oAddonArticleInfo.setSortOrder(o.getInt("sortOrder"));
					oAddonArticleInfo.setOldSortOrder(o.getInt("oldSortOrder"));
					oAddonArticleInfo.setAddonPrice(o.getDouble("addonPrice"));

					oAddonArticleDAO.updateAddonArticleItem(oAddonArticleInfo);

					// Get items from the db
					oArticleAddons = oAddonArticleDAO.getAddonArticleItems(iArticleToGroupId);

					response.put("status", "success");
					response.put("total", "1");
					response.put("page", "1");
					response.put("records", oArticleAddons.size());
					response.put("rows", oArticleAddons);
				}
			} else {
				response.put("status", "error");
				response.put("message", "Empty input!");
			}

		} catch (Exception e) {
			response.put("status", "error");
			response.put("message", e.getMessage());
		} finally {
			session.closeDBSession();
		}

		logger.info("returning value from editing AddonToArticle");
		logger.info(response);
		return response;
	}

	/**
	 * delete ArticleToGroup
	 * 
	 * @param
	 * @return
	 */
	private JSONObject deleteArticleAddon(JSONArray oRequestInput) {
		logger.info("Getting articles");

		JSONObject response = new JSONObject();
		CDBSession session = CDBSessionFactory.instance.createSession();

		try {
			if (oRequestInput != null && oRequestInput.size() > 0) {
				for (int i = 0, n = oRequestInput.size(); i < n; i++) {
					JSONObject o = oRequestInput.getJSONObject(i);

					if (o.isNullObject()) {
						continue;
					}

					MBAddonArticleInfo oAddonArticleInfo = new MBAddonArticleInfo();
					AddonArticleDAO oAddonArticleDAO = new AddonArticleDAO();

					oAddonArticleInfo.setId(o.getInt("id"));

					oAddonArticleDAO.deleteAddonArticleItem(oAddonArticleInfo);

					response.put("status", "success");
				}
			} else {
				response.put("status", "error");
				response.put("message", "Empty input!");
			}

		} catch (Exception e) {
			response.put("status", "error");
			response.put("message", e.getMessage());
		} finally {
			session.closeDBSession();
		}

		logger.info("returning value from editing addongroup");
		logger.info(response);
		return response;
	}

	/**
	 * Adds ArticleAddon
	 * 
	 * @param
	 * @return
	 */
	private JSONObject addAddonItemsToReceipt(JSONArray oRequestInput) {
		logger.info("Add article 2 group");

		JSONObject response = new JSONObject();
		CDBSession session = CDBSessionFactory.instance.createSession();

		ReceiptManager receiptMgr = new ReceiptManager(session);
		MaterialManager matMgr = new MaterialManager(session);

		try {
			if (oRequestInput != null && oRequestInput.size() > 0) {
				ReceiptEntity receipt = receiptMgr
						.findOrCreate((UserEntity) CContext.getInstance().get(CContext.CURRENT_USER), null, false);

				for (int i = 0, n = oRequestInput.size(); i < n; i++) {
					JSONObject o = oRequestInput.getJSONObject(i);

					if (o.isNullObject()) {
						continue;
					}
					// get items from incoming object
					String itemCode = o.getString("itemCode");
					BigDecimal quantity = BigDecimal.valueOf(o.getInt("quantity"));
					BigDecimal price = BigDecimal.valueOf(o.getDouble("price"));
					
					// Create a salesItem
					SalesItemEntity salesItem = new SalesItemEntity();
					salesItem.setTypeCode(SalesItemEntity.SalesItemTypeCode.MATERIAL);
//					salesItem.setQuantityTypeCode("-1");
					salesItem.setQuantityTypeCode("EA");
					salesItem.setQuantity(quantity);
					
					salesItem.setGrossAmount(price);
					salesItem.setPaymentGrossAmount(price);
					salesItem.setUnitGrossAmountOrigin(price);
					//salesItem.setNetAmount(price);
					salesItem.setUnitGrossAmount(price);
					salesItem.setNotRoundedPaymentGrossAmount(price);
					salesItem.setUnitPriceChanged(true);

					// Get material from db
					MaterialEntity oAddonMaterial = matMgr.readMaterialByExternalID(itemCode);

					if (oAddonMaterial != null) {
						salesItem.setMaterial(oAddonMaterial);
						salesItem.setTaxRateTypeCode(oAddonMaterial.getTaxRateTypeCode());
						salesItem.setDescription(oAddonMaterial.getDescription());
					}

					receiptMgr.addSalesItems(receipt, salesItem);
				}
				
				receiptMgr.update(receipt);
				ReceiptManager.updateDB(session);

				UIEventDispatcher.INSTANCE.dispatchAction("RECEIPT_REFRESH", null, receipt);
				response.put("status", "success");
				response.put("message", "Addons added!");
			} else {
				response.put("status", "error");
				response.put("message", "Empty input!");
			}

		} catch (Exception e) {
			response.put("status", "error");
			response.put("message", e.getMessage());
		} 
//		finally {
//			session.closeDBSession();
//		}

		logger.info("returning value from adding article addon");
		logger.info(response);

		return response;
	}

	/**
	 * get user info
	 * 
	 * @param
	 * @return
	 */
	private JSONObject getCurrentUserInfo() {
		logger.info("Entering getUserInfo");

		JSONObject response = new JSONObject();

		UserEntity user = new UserEntity();
		try {
			user = (UserEntity) CContext.getInstance().get(CContext.CURRENT_USER);
		} catch (Exception e) {
			logger.error("Error getting user entity", e);
		}

		if (user != null) {
			List<PermissionEntity> oListOfPermissions = user.getPermissions();
			response.put("status", "success");
			response.put("status", "success");
			response.put("total", "1");
			response.put("page", "1");
			response.put("records", oListOfPermissions.size());
			response.put("rows", oListOfPermissions);

			logger.info(user);
		} else {
			response.put("status", "error");
		}

		logger.info("returning value from getUserInfo");
		logger.info(response);
		return response;
	}
}
