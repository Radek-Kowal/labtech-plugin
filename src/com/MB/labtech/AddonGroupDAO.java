package com.MB.labtech;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.log4j.Logger;

import com.sap.scco.ap.pos.dao.CDBSession;
import com.sap.scco.ap.pos.dao.CDBSessionFactory;

/**
 * This Data Access Object will handle the DB CRUD Operations
 * 
 * @author RK
 *
 */
public class AddonGroupDAO {
	private static final Logger logger = Logger.getLogger(AddonGroupDAO.class);
	private static final String MB_ADDON_GROUPS_TABLE_NAME = "MB_ADDON_GROUPS";

	/**
	 * Create the new DB table, if not existing
	 */
	public void setupTableForAddonGroups() {
		CDBSession session = CDBSessionFactory.instance.createSession();
		try {
			// Try to create the new table. If this fails, the table is most likely already
			// existing
			session.beginTransaction();
			EntityManager em = session.getEM();

			String sQuery = "CREATE TABLE " + MB_ADDON_GROUPS_TABLE_NAME + " ("
					+ "GroupId INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),"
					+ "GroupName VARCHAR(254) NOT NULL,"
					+ "GroupDescription VARCHAR(254), CONSTRAINT primary_key PRIMARY KEY (GroupId) )";

			logger.debug(sQuery);

			Query qAddonGroupsSQLTableCreate = em.createNativeQuery(sQuery);

			qAddonGroupsSQLTableCreate.executeUpdate();

			session.commitTransaction();
			logger.info("Created table for Addon Groups.");
		} catch (Exception e) {
			session.rollbackDBSession();
			logger.info(e.getMessage());
			logger.info(e.getStackTrace());
			logger.warn("Addon Groups table already existing.");
		} finally {
			session.closeDBSession();
		}
	}

	public MBArticleGroupInfo addOrUpdateAddonGroup(MBArticleGroupInfo oGroupItem) {
		logger.info("GetGroupItem : " + oGroupItem.getGroupId());
		MBArticleGroupInfo oGroupItemFromDB = getAddonGroupItem(oGroupItem.getGroupId());

		if (oGroupItemFromDB != null) {
			return updateAddonGroupItem(oGroupItem);
		} else {
			return createAddonGroupItem(oGroupItem);
		}
	}

	public MBArticleGroupInfo updateAddonGroupItem(MBArticleGroupInfo oGroupItem) {
		CDBSession session = CDBSessionFactory.instance.createSession();
		try {
			session.beginTransaction();
			EntityManager em = session.getEM();
			Query q = em.createNativeQuery(
					"UPDATE " + MB_ADDON_GROUPS_TABLE_NAME + " SET GroupName=?, GroupDescription=? WHERE GroupId=?");
			q.setParameter(1, oGroupItem.getGroupName());
			q.setParameter(2, oGroupItem.getGroupDescription());
			q.setParameter(3, oGroupItem.getGroupId());

			q.executeUpdate();
			session.commitTransaction();
		} catch (RuntimeException e) {
			session.rollbackDBSession();
			throw e;
		} finally {
			session.closeDBSession();
		}

		return oGroupItem;
	}
	
	public MBArticleGroupInfo deleteAddonGroupItem(MBArticleGroupInfo oGroupItem) {
		CDBSession session = CDBSessionFactory.instance.createSession();
		try {
			session.beginTransaction();
			EntityManager em = session.getEM();
			Query q = em.createNativeQuery(
					"DELETE FROM " + MB_ADDON_GROUPS_TABLE_NAME + " WHERE GroupId=?");

			q.setParameter(1, oGroupItem.getGroupId());

			q.executeUpdate();
			session.commitTransaction();
		} catch (RuntimeException e) {
			session.rollbackDBSession();
			throw e;
		} finally {
			session.closeDBSession();
		}

		return oGroupItem;
	}

	public MBArticleGroupInfo createAddonGroupItem(MBArticleGroupInfo oGroupItem) {
		CDBSession session = CDBSessionFactory.instance.createSession();
		try {
			session.beginTransaction();
			EntityManager em = session.getEM();
			Query q = em.createNativeQuery(
					"INSERT INTO " + MB_ADDON_GROUPS_TABLE_NAME + " (GroupName, GroupDescription) VALUES(?,?)");

			q.setParameter(1, oGroupItem.getGroupName());
			q.setParameter(2, oGroupItem.getGroupDescription());

			logger.info("createAddonGroupItem : " + q.toString());

			q.executeUpdate();
			session.commitTransaction();
		} catch (Exception e) {
			session.rollbackDBSession();
			throw e;
		} finally {
			session.closeDBSession();
		}

		return oGroupItem;
	}

	public MBArticleGroupInfo getAddonGroupItem(Integer iGroupId) {
		CDBSession session = CDBSessionFactory.instance.createSession();
		try {
			EntityManager em = session.getEM();
			Query q = em.createNativeQuery("SELECT * FROM " + MB_ADDON_GROUPS_TABLE_NAME + " where GroupId=?");
			q.setParameter(1, iGroupId);

			logger.info("getAddonGroupItem : " + q.toString());

			@SuppressWarnings("unchecked")
			List<Object[]> results = q.getResultList();

			if (results.isEmpty()) {
				return null;
			}

			return mapResultRow(results.get(0));
		} finally {
			session.closeDBSession();
		}
	}

	public List<MBArticleGroupInfo> getAddonGroupItems() {
		List<MBArticleGroupInfo> resultList = new ArrayList<>();

		CDBSession session = CDBSessionFactory.instance.createSession();
		try {
			EntityManager em = session.getEM();
			Query q = em.createNativeQuery("SELECT * FROM " + MB_ADDON_GROUPS_TABLE_NAME);
//			q.setParameter(1, iGroupId);

//			logger.info("parameter added: " + iGroupId);
			logger.info("getAddonGroupItems : " + q.toString());

			@SuppressWarnings("unchecked")
			List<Object[]> results = q.getResultList();
			for (Object[] result : results) {
				resultList.add(mapResultRow(result));
			}
		} catch (ClassCastException e) {
			logger.info(e.getMessage());
		} catch (Exception e) {
			logger.warn(e.getMessage());
			setupTableForAddonGroups();
		} finally {
			session.closeDBSession();
		}

		return resultList;
	}

	public void dropAll() {
		CDBSession session = CDBSessionFactory.instance.createSession();
		try {
			session.beginTransaction();
			EntityManager em = session.getEM();
			// Query q = em.createNativeQuery("DELETE FROM " + MB_TOWNS_TABLE_NAME );
			Query q = em.createNativeQuery("DROP TABLE " + MB_ADDON_GROUPS_TABLE_NAME);
			q.executeUpdate();
			session.commitTransaction();
		} catch (RuntimeException e) {
			session.rollbackDBSession();
			throw e;
		} finally {
			session.closeDBSession();
		}
	}

	private MBArticleGroupInfo mapResultRow(Object[] result) {
		MBArticleGroupInfo oGroupItem = new MBArticleGroupInfo();

		oGroupItem.setGroupId(Integer.parseInt(result[0].toString())); 
		oGroupItem.setGroupName(result[1].toString());
		oGroupItem.setGroupDescription((String) result[2]);

		return oGroupItem;
	}
}
