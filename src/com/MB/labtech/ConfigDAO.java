package com.MB.labtech;

public interface ConfigDAO {
	String getProperty(String key, String defaultValue);
}
