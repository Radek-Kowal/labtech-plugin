package com.MB.labtech;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.apache.log4j.Logger;

import com.MB.labtech.Commons.AddonDefault;
import com.sap.scco.ap.pos.dao.CDBSession;
import com.sap.scco.ap.pos.dao.CDBSessionFactory;

/**
 * This Data Access Object will handle the DB CRUD Operations
 * 
 * @author RK
 *
 */
public class AddonArticleDAO {
	private static final Logger logger = Logger.getLogger(AddonArticleDAO.class);
	private static final String MB_ADDON_ARTICLES_TABLE_NAME = "MB_ADDON_ARTICLES";
	private static final String MB_ADDON_ARTICLE2GROUP_TABLE_NAME = "MB_ADDON_ARTICLE2GROUP";
	private static final String MB_ADDON_GROUPS_TABLE_NAME = "MB_ADDON_GROUPS";

	/**
	 * Create the new DB table, if not existing
	 */
	public void setupTableForAddonArticles() {
		CDBSession session = CDBSessionFactory.instance.createSession();
		try {
			// Try to create the new table. If this fails, the table is most likely already
			// existing
			session.beginTransaction();
			EntityManager em = session.getEM();

			String sQuery = "CREATE TABLE " + MB_ADDON_ARTICLES_TABLE_NAME + " ("
					+ "Id INTEGER NOT NULL PRIMARY KEY GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),"
					+ "AddonArticle VARCHAR(255) NOT NULL,"
					+ "ArticleToGroupId INTEGER REFERENCES MB_ADDON_ARTICLE2GROUP(Id)," + "SortOrder INTEGER NOT NULL,"
					+ "AddonDefault CHAR(1) NOT NULL CHECK (AddonDefault IN ('Y', 'N') )," + "AddonPrice DECIMAL(19,6),"
					+ "UserId VARCHAR(36), " + "UpdateDate TIMESTAMP, "
					+ "CONSTRAINT unique_addon_group UNIQUE (AddonArticle, ArticleToGroupId),"
					+ "CONSTRAINT unique_addon_order UNIQUE (ArticleToGroupId, SortOrder),"
					+ "CONSTRAINT unique_addon_default UNIQUE (ArticleToGroupId, AddonDefault)" + ")";

			logger.debug(sQuery);

			Query qAddonGroupsSQLTableCreate = em.createNativeQuery(sQuery);

			qAddonGroupsSQLTableCreate.executeUpdate();

			session.commitTransaction();
			logger.info("Created table for Addon Articles.");
		} catch (PersistenceException e) {
			session.rollbackDBSession();
			String msg = e.getMessage();
			if (msg.contains("already exists")) {
				logger.warn(String.format("%s table already existing.", MB_ADDON_ARTICLES_TABLE_NAME));
			}
		} catch (Exception e) {
			session.rollbackDBSession();
			logger.error(e.getMessage());
			logger.error(e.getStackTrace());
		} finally {
			session.closeDBSession();
		}
	}

	public MBAddonArticleInfo addOrUpdateAddonArticle(MBAddonArticleInfo oAddonArticleItem) {
		logger.info("GetAddonArticleItem : " + oAddonArticleItem.getAddonArticle());
		MBAddonArticleInfo oAddonArticleItemFromDB = getAddonArticleItem(oAddonArticleItem.getId());

		if (oAddonArticleItemFromDB != null) {
			return updateAddonArticleItem(oAddonArticleItem);
		} else {
			return createAddonArticleItem(oAddonArticleItem);
		}
	}

	public MBAddonArticleInfo updateAddonArticleItem(MBAddonArticleInfo oAddonArticleItem) {
		CDBSession session = CDBSessionFactory.instance.createSession();
		try {
			session.beginTransaction();
			EntityManager em = session.getEM();
			Query q;

			Integer newSortOrder = oAddonArticleItem.getSortOrder();
			Integer oldSortOrder = oAddonArticleItem.getOldSortOrder();

			// initial update of props AND SortOrder to 0 in order to avoid unique
			// constraint violation
			q = em.createNativeQuery("UPDATE " + MB_ADDON_ARTICLES_TABLE_NAME
					+ " SET AddonArticle=?, AddonDefault=?, AddonPrice=?, SortOrder=0 where Id=?");

			q.setParameter(1, oAddonArticleItem.getAddonArticle());
			q.setParameter(2, oAddonArticleItem.getAddonDefault());
			q.setParameter(3, oAddonArticleItem.getAddonPrice());
			q.setParameter(4, oAddonArticleItem.getId());
			q.executeUpdate();

			if (newSortOrder < oldSortOrder) {
				q = em.createNativeQuery("UPDATE " + MB_ADDON_ARTICLES_TABLE_NAME
						+ " SET SortOrder = SortOrder + 1 WHERE SortOrder >= ? AND SortOrder < ? AND ArticleToGroupId=?");
				q.setParameter(1, newSortOrder);
				q.setParameter(2, oldSortOrder);
				q.setParameter(3, oAddonArticleItem.getArticleToGroupId());
				q.executeUpdate();
			} else if (newSortOrder > oldSortOrder) {
				q = em.createNativeQuery("UPDATE " + MB_ADDON_ARTICLES_TABLE_NAME
						+ " SET SortOrder = SortOrder - 1 WHERE SortOrder > ? AND SortOrder <= ? AND ArticleToGroupId=?");
				q.setParameter(1, oldSortOrder);
				q.setParameter(2, newSortOrder);
				q.setParameter(3, oAddonArticleItem.getArticleToGroupId());
				q.executeUpdate();
			}

			// final update to set the correct sort order
			q = em.createNativeQuery("UPDATE " + MB_ADDON_ARTICLES_TABLE_NAME + " SET SortOrder=? where Id=?");

			q.setParameter(1, newSortOrder);
			q.setParameter(2, oAddonArticleItem.getId());

			q.executeUpdate();

			session.commitTransaction();
		} catch (PersistenceException e) {
			session.rollbackDBSession();
			String msg = e.getMessage();
			if (msg.contains("UNIQUE_ADDON_ORDER")) {
				throw new RuntimeException("You cannot enter the same 'SORT ORDER' for the second time!");
			} else if (msg.contains("UNIQUE_ADDON_GROUP")) {
				throw new RuntimeException(
						"You cannot enter the same 'ARTICLE' for the same group for the second time!");
			} else if (msg.contains("UNIQUE_ADDON_DEFAULT")) {
				throw new RuntimeException(
						"You cannot set the same 'ARTICLE' as 'DEFAULT' for the same group for the second time!");
			}
		} catch (RuntimeException e) {
			session.rollbackDBSession();
			throw e;
		} finally {
			session.closeDBSession();
		}

		return oAddonArticleItem;
	}

	public MBAddonArticleInfo createAddonArticleItem(MBAddonArticleInfo oAddoneArticleItem) {
		CDBSession session = CDBSessionFactory.instance.createSession();
		try {
			session.beginTransaction();
			EntityManager em = session.getEM();
			Query q = em.createNativeQuery("INSERT INTO " + MB_ADDON_ARTICLES_TABLE_NAME
					+ " ( AddonArticle, ArticleToGroupId, SortOrder, AddonDefault, AddonPrice, UserId, UpdateDate ) VALUES(?,?,?,?,?,?,?)");

			q.setParameter(1, oAddoneArticleItem.getAddonArticle());
			q.setParameter(2, oAddoneArticleItem.getArticleToGroupId());
			q.setParameter(3, oAddoneArticleItem.getSortOrder());
			q.setParameter(4, oAddoneArticleItem.getAddonDefault());
			q.setParameter(5, oAddoneArticleItem.getAddonPrice());
			q.setParameter(6, oAddoneArticleItem.getUserId());
			q.setParameter(7, oAddoneArticleItem.getUpdateDate());

			logger.info(String.format("createAddonArticleItem! article: [%s], group: [%s]",
					oAddoneArticleItem.getAddonArticle(), oAddoneArticleItem.getArticleToGroupId()));

			q.executeUpdate();
			session.commitTransaction();
		} catch (PersistenceException e) {
			session.rollbackDBSession();
			String msg = e.getMessage();
			if (msg.contains("UNIQUE_ADDON_ORDER")) {
				throw new RuntimeException("You cannot enter the same 'SORT ORDER' for the second time!");
			} else if (msg.contains("UNIQUE_ADDON_GROUP")) {
				throw new RuntimeException(
						"You cannot enter the same 'ARTICLE' for the same group for the second time!");
			} else if (msg.contains("UNIQUE_ADDON_DEFAULT")) {
				throw new RuntimeException(
						"You cannot set the same 'ARTICLE' as 'DEFAULT' for the same group for the second time!");
			}
		} catch (Exception e) {
			session.rollbackDBSession();
			throw e;
		} finally {
			session.closeDBSession();
		}

		return oAddoneArticleItem;
	}

	public MBAddonArticleInfo getAddonArticleItem(Integer id) {
		CDBSession session = CDBSessionFactory.instance.createSession();
		try {
			EntityManager em = session.getEM();
			Query q = em.createNativeQuery("SELECT * FROM " + MB_ADDON_ARTICLES_TABLE_NAME + " where Id=?");
			q.setParameter(1, id);

			logger.info("getAddonArticleItem : " + q.toString());

			@SuppressWarnings("unchecked")
			List<Object[]> results = q.getResultList();

			if (results.isEmpty()) {
				return null;
			}

			return mapResultRow(results.get(0));
		} finally {
			session.closeDBSession();
		}
	}

	public List<MBAddonArticleInfo> getAddonArticleItems(Integer iAddonGroupId) {
		List<MBAddonArticleInfo> resultList = new ArrayList<>();
		CDBSession session = CDBSessionFactory.instance.createSession();

		try {
			EntityManager em = session.getEM();
			Query q = em
					.createNativeQuery("SELECT * FROM " + MB_ADDON_ARTICLES_TABLE_NAME + " where ArticleToGroupId=?");
//			q.setParameter(1, sArticle);
			q.setParameter(1, iAddonGroupId);

			logger.info("getAddonArticleItem : " + q.toString());

			@SuppressWarnings("unchecked")
			List<Object[]> results = q.getResultList();
			for (Object[] result : results) {
				resultList.add(mapResultRow(result));
			}
		} finally {
			session.closeDBSession();
		}

		return resultList;
	}

	public List<MBAddonArticleInfo> getAddonArticleItems() {
		List<MBAddonArticleInfo> resultList = new ArrayList<>();

		CDBSession session = CDBSessionFactory.instance.createSession();
		try {
			EntityManager em = session.getEM();
			Query q = em.createNativeQuery("SELECT * FROM " + MB_ADDON_ARTICLES_TABLE_NAME);

			logger.info("getAddonGroupItems : " + q.toString());

			@SuppressWarnings("unchecked")
			List<Object[]> results = q.getResultList();
			for (Object[] result : results) {
				resultList.add(mapResultRow(result));
			}
		} catch (ClassCastException e) {
			logger.info(e.getMessage());
		} catch (Exception e) {
			logger.warn(e.getMessage());
			setupTableForAddonArticles();
		} finally {
			session.closeDBSession();
		}

		return resultList;
	}

	public List<MBAddonArticleInfo> getAddonArticleItems(String sParentArticle) {
		List<MBAddonArticleInfo> resultList = new ArrayList<>();
		CDBSession session = CDBSessionFactory.instance.createSession();
		try {
			EntityManager em = session.getEM();
			Query q = em.createNativeQuery("SELECT AA.*, M.Description FROM ("
					+ "	SELECT * FROM MB_ADDON_ARTICLES WHERE ArticleToGroupId IN (" + "		SELECT  MA.Id "
					+ "		FROM MB_ADDON_ARTICLE2GROUP MA JOIN MB_ADDON_GROUPS MG ON MA.GroupId = MG.GroupId WHERE MA.ParentArticle=?"
					+ "	)" + ") AA JOIN MATERIAL M ON AA.AddonArticle = M.ExternalId");

			q.setParameter(1, sParentArticle);
			logger.info("getAddonGroupItems : " + q.toString());

			@SuppressWarnings("unchecked")
			List<Object[]> results = q.getResultList();
			for (Object[] result : results) {
				resultList.add(mapResultRow(result));
			}
		} catch (ClassCastException e) {
			logger.info(e.getMessage());
		} catch (Exception e) {
			logger.warn(e.getMessage());
			setupTableForAddonArticles();
		} finally {
			session.closeDBSession();
		}

		return resultList;
	}

	public MBAddonArticleInfo deleteAddonArticleItem(MBAddonArticleInfo oAddonToArticleItem) {
		CDBSession session = CDBSessionFactory.instance.createSession();
		try {
			session.beginTransaction();
			EntityManager em = session.getEM();
			Query q = em.createNativeQuery("DELETE FROM " + MB_ADDON_ARTICLES_TABLE_NAME + " WHERE Id=?");

			q.setParameter(1, oAddonToArticleItem.getId());

			q.executeUpdate();
			session.commitTransaction();
		} catch (RuntimeException e) {
			session.rollbackDBSession();
			throw e;
		} finally {
			session.closeDBSession();
		}

		return oAddonToArticleItem;
	}

	public void dropAll() {
		CDBSession session = CDBSessionFactory.instance.createSession();
		try {
			session.beginTransaction();
			EntityManager em = session.getEM();
			// Query q = em.createNativeQuery("DELETE FROM " + MB_TOWNS_TABLE_NAME );
			Query q = em.createNativeQuery("DROP TABLE " + MB_ADDON_ARTICLES_TABLE_NAME);
			q.executeUpdate();
			session.commitTransaction();
		} catch (RuntimeException e) {
			session.rollbackDBSession();
			throw e;
		} finally {
			session.closeDBSession();
		}
	}

	private MBAddonArticleInfo mapResultRow(Object[] result) {
		MBAddonArticleInfo oAddonArticleItem = new MBAddonArticleInfo();

		oAddonArticleItem.setId(Integer.parseInt(result[0].toString()));
		oAddonArticleItem.setAddonArticle(result[1].toString());
		oAddonArticleItem.setArticleToGroupId((Integer) result[2]);
		oAddonArticleItem.setSortOrder((Integer) result[3]);
		oAddonArticleItem.setAddonDefault(result[4].toString());
		BigDecimal price = (BigDecimal) result[5];
		oAddonArticleItem.setAddonPrice(price.doubleValue());
		oAddonArticleItem.setUserId((String) result[6]);
		oAddonArticleItem.setUpdateDate((Timestamp) result[7]);
		if (result.length > 8) {
			oAddonArticleItem.setAddonDescription(result[8].toString());
		}

		return oAddonArticleItem;
	}
}
