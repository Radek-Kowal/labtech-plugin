package com.MB.labtech;

import java.math.BigDecimal;

/**
 * structure for article item entry 
 * @author RK 
 */
public class MBArticleInfo {
	private String itemCode;
	private String itemName;
	private BigDecimal unitPrice;
	private Boolean isParent;
	
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public BigDecimal getUnitPrice() {
		return unitPrice;
	}
	public Boolean getIsParent() {
		return isParent;
	}
	public void setIsParent(Boolean isParent) {
		this.isParent = isParent;
	}
}
