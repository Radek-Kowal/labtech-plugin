/*!
 * handle jQgrid ArticleAddons grid
 *
 */

let articleAddonsModule = (function() {
    'use strict';
 
    let allData = [];
    let addonArticlesForSelect = {};
    let sortOrderForSelect = {};
    
    function gridReload() {
    	$('#articleAddonsGrid').jqGrid('clearGridData');
    	$('#articleAddonsGrid').jqGrid('setGridParam', {data: allData});
    	$('#articleAddonsGrid').trigger('reloadGrid');
    	getSortOrder(allData);
    }
    
    function resetGrid() {
    	$("#articleAddonsGrid").jqGrid('GridUnload');
        // reset the "value" property of the editoptions property to the initial value
    	if (ADDONARTICLESGROUPID && ADDONARTICLESGROUPID != null && ADDONARTICLESGROUPID !== "") {   		
    		createArticleAddonsGrid(ADDONARTICLESGROUPID);
    	} else {
    		let rowid = $("#articleGroupsGrid").jqGrid("getGridParam","selrow");
  			if( rowid != null ) {
  				let rowData = $("#articleGroupsGrid").jqGrid("getRowData",parseInt(rowid,10));
  				ADDONARTICLESGROUPID = rowData["addonGroupId"];
  				console.log(`This is addonGroupId: ${ADDONARTICLESGROUPID} in resetGrid() for articleAddonsGrid!`);
  				createArticleAddonsGrid(ADDONARTICLESGROUPID);
  			}
    	}
    }
    
    function loadAddonArticles() {
		showInProgressDialog();

		$.ajax({
			"url" : "PluginServlet?action=getArticles",
			"dataType" : "JSON",
			"type" : "GET",
			"async": false
		}).success(function (data) {
			hideInProgressDialog();
			console.log("loadAddonArticles: ");
			console.log(data);
			
			if (data && data.rows && data.rows.length > 0) { 
	  			addonArticlesForSelect = {};
				let addonArticles = data.rows.filter((i) => { return i.isParent === false; });
				console.log("addon Articles: ");
				console.log(JSON.stringify(addonArticles));
				addonArticles.forEach(prepareAddonArticles);
			}
		}).fail(function () {
			hideInProgessDialog();
			alert("Error!");
		});
	}
    
    function prepareAddonArticles(element, index, array) {
		addonArticlesForSelect[element["itemCode"].toString()] = element["itemName"];
  	}
  	
  	function getArticleAddonsFromServer(addonArticleGroupId) {
  		$.ajax({
  			type :"GET",
  		    contentType :"application/json; charset=utf-8",
  		    dataType :"json",
  		    url: "PluginServlet?action=getArticleAddons&addonArticleGroupId=" + addonArticleGroupId,
  		    async: false
  		}).success((data) => {
  			hideInProgessDialog();
  			
  			allData = data.rows;
  			
  			loadAddonArticles();
  			
  			gridReload();
  		}).fail((xhr, status, error) => {
  			hideInProgessDialog();
  			showMessageDialog("Error in backend call: " + + xhr.responseText, {
  				"type" : "error"
  			});		
  		});
  	}
  	
  	function getSortOrder(data) {
  		if (data && data.length > 0) {
  			sortOrderForSelect[1] = data.length + 1;
  		} else {
  			sortOrderForSelect[1] = 1;
  		}
  	}
  	
  	function createArticleAddonsGrid(addonArticleGroupId) {
  		// show the buttons
  		$("#addAddonToArticle").show();
  		$("#editAddonToArticle").show();
  		$("#deleteAddonToArticle").show();
  		
  		let groupIdForDelUpdate = "0";
  		
  		getArticleAddonsFromServer(addonArticleGroupId);
  		console.log(allData);
  		
  		let idForUpdate, oldSortOrder, newSortOrder;

  		// Prepare jQgrid
  		$("#articleAddonsGrid").jqGrid({
  			data: allData,
  			datatype: "local",
  			colNames : ["Id",
  				"Addon Article",
  				"Article Group",
  				"Addon Sort Order",
  				"Default?",
  				"Price"],
  			colModel : [{
  					name : "id",
  					index: "id",
  					hidden: true
  				},{
  					name : "addonArticle",
  					index: "addonArticle",
  					width : 90,
//  					editable: true,
//  					edittype : "select", 
//  	                formatter : "select",
//
//  	                editoptions:{value: addonArticlesForSelect}
  				}, {
  					name: "articleToGroupId",
  					index: "articleToGroupId",
  					hidden: true
  				}, {
  					name : "sortOrder",
  					index: "sortOrder",
  					width : 50,
  					editable: true  					
  				}, {
  					name : "addonDefault",
  					index :"addonDefault",
  	                align : "center",
  	                width : 35,
  	                editable : true,
  	                cellEdit : true,
  	                edittype : "select", 
  	                formatter : "select",

  	                editoptions:{value: getDefaultSelectOptions()}
  				}, {
  					name : "addonPrice",
  					index: "addonPrice",
  					width : 90,
  					editable: true,
  					editrules:{
  						number:true, 
  						required:true
  					}
  				}
  			],
  			beforeSelectRow: function(id) {
				var cm = $('#articleAddonsGrid').jqGrid('getColProp',"addonArticle");
				cm.editable = true;// Editable
				cm.edittype = "select";
				cm.formatter = "select";
				cm.editoptions = {
					aysnc: false, 
					dataUrl: "PluginServlet?action=getArticles",
					buildSelect: function (data) {  
						var responseJSON = $.parseJSON(data);
						var s = "<select>";
						s += '<option value="0">--No Article--</option>';
						responseJSON.rows.map((o) => {
							if (o.isParent === false) {
								console.log(o.itemCode);
								s += '<option value="' + o.itemCode + '">' + o.itemName + '</option>';
							}
						});
						return s + "</select>";
					}
				}
				return true;
			},
  			rowNum : 10,
  			rowList : [10, 20, 30],
  			autowidth : false,
  			width : 600,
  			rownumbers : true,
  			pager : "#articleAddonsPager",
  			sortname : "sortOrder",
  			viewrecords : true,
  			sortorder : "asc",
  			caption : "Article Addons",
  			emptyrecords : "No Addons",
  			loadonce : false,
  			toolbar: [true,"top"],
  			jsonReader : {
  				root : "rows",
  				page : "page",
  				total : "total",
  				records : "records",
  				repeatitems : false,
  				cell : "cell",
  				id : "id"
  			}
  		});	
  		
  		$("#articleAddonsGrid").sortableRows();   
  		$("#articleAddonsGrid").jqGrid("gridDnD");
  		
  		function getDefaultSelectOptions(){
  			let options = { "Y": "Yes", "N": "No" };
  			return options;
  		}
  		
  		let jsonAddAddonOptions = {
  		    type :"POST",
  		    contentType :"application/json; charset=utf-8",
  		    dataType :"json",
  		    "url": "PluginServlet?action=addArticleAddon",
  		    "async": false,
  			"success": function(data){
  				hideInProgessDialog(); 	
  				showBackendCallStatus(data, "New addon for article group added successfully!");
  			},
  			"error": function(xhr, status, error){
  				hideInProgessDialog();
  				showMessageDialog("Error in backend call: " + xhr.responseText, {
  					"type" : "error"
  				});	
  			}
  		};
  		
  		let jsonEditAddonOptions = {
  		    type :"POST",
  		    contentType :"application/json; charset=utf-8",
  		    dataType :"json",
  		    "url": "PluginServlet?action=editArticleAddon",
  		    "async": false,
  			"success": function(data){
  				hideInProgessDialog(); 	
  				showBackendCallStatus(data, "Addon edited successfully!");  				
  			},
  			"error": function(xhr, status, error){
  				hideInProgessDialog();
  				showMessageDialog("Error in backend call: " + xhr.responseText, {
  					"type" : "error"
  				});	
  			}
  		};

  		let editAddonFormOptions = {
  			height:200,
  			width: 400,
  			processData: "Processing...",
  			ajaxEditOptions: jsonEditAddonOptions,
  			serializeEditData: (postdata) => {
  				let rowid = $("#articleAddonsGrid").jqGrid("getGridParam","selrow");
  				let output = [];
  	  			if( rowid != null ) {
  	  				let rowData = $("#articleAddonsGrid").jqGrid("getRowData",parseInt(rowid,10));
  	  				idForUpdate = rowData["id"];
  	  				
  	  				let itemCode = $("#articleSelectBox").val();
  	  				let groupId = $("#addonGroupId").val();
  	  				
  	  				oldSortOrder = rowData["sortOrder"];
  	  				newSortOrder = postdata.sortOrder || oldSortOrder;
  	  				
  	  				let addonSingle = $("#single").val();
  	  				
  	  				output.push({	
  	  					id: idForUpdate,
  	  					addonArticle: postdata.addonArticle,
	  	  				articleToGroupId: ADDONARTICLESGROUPID,
	  			    	sortOrder: newSortOrder,
	  			    	oldSortOrder: oldSortOrder,
	  			    	addonDefault: postdata.addonDefault,
	  			    	addonPrice: postdata.addonPrice
  	  				});
  	  			}
  				 return JSON.stringify(output);
  			},
  			recreateForm : true,
  			closeAfterEdit : true,
  			reloadAfterSubmit : true,
  			beforeShowForm: (formid) => {
  				setSortOrderColumnProperties(); 
  			},
//  			afterSubmit: (response, postdata) => {
//  				getArticleAddonsFromServer(ADDONARTICLESGROUPID);
//  				return [true,"",null];
//  			},
  			onClose: function () {
  				resetGrid();
            }
  		}
  		
  		let addAddonFormOptions = {
  			addCaption: "Add Addon",
  			width: "auto",
  			height:200,
  			processData: "Processing...",
  			ajaxEditOptions: jsonAddAddonOptions,
  			serializeEditData: (postdata) => {
  				let itemCode = $("#addonArticle").val();  				
  				let sortOrder = sortOrderForSelect[1];
  				let output = [];
  			    output.push({	    	
  			    	addonArticle: itemCode,
  			    	articleToGroupId: ADDONARTICLESGROUPID,
  			    	sortOrder: postdata.sortOrder,
  			    	addonDefault: postdata.addonDefault,
  			    	addonPrice: postdata.addonPrice
  			    });
  			    return JSON.stringify(output);
  			},
  			recreateForm : true,
  			closeAfterAdd : true,
  			reloadAfterSubmit : true,
  			beforeShowForm: (formid) => {
  				setAddonArticleColumnProperties();
  				setSortOrderColumnProperties("add"); 
  			},
//  			afterSubmit: (response, postdata) => {
//  				console.log("IN afterSubmit for addAddonFormOptions");
//  				getArticleAddonsFromServer(ADDONARTICLESGROUPID);
//  				return [true,"",null];
//  			},
  			onClose: function () {
  				resetGrid();
            }
  		}
  		
  		function setAddonArticleColumnProperties() {
  			var cm = $('#articleAddonsGrid').jqGrid('getColProp',"addonArticle");
			cm.editable = true;// Editable
			cm.edittype = "select";
			cm.formatter = "select";
			cm.editoptions = {
				aysnc: false, 
				dataUrl: "PluginServlet?action=getArticles",
				buildSelect: function (data) {  
					var responseJSON = $.parseJSON(data);
					var s = "<select>";
					s += '<option value="0">--No Article--</option>';
					responseJSON.rows.map((o) => {
						if (o.isParent === false) {
							console.log(o.itemCode);
							s += '<option value="' + o.itemCode + '">' + o.itemName + '</option>';
						}
					});
					return s + "</select>";
				}
			}
  		}
  		
  		function setSortOrderColumnProperties(mode) {
  			var cm = $('#articleAddonsGrid').jqGrid('getColProp',"sortOrder");
  			console.log("Sort order for select: " + sortOrderForSelect[1]);
  			
			cm.editrules = {
				number:true, 
				required:true, 
				integer: true, 
				minValue: 1,
				maxValue: (sortOrderForSelect[1] === 1) ? 1 : (sortOrderForSelect[1])  //sortOrderForSelect shows next possible value
			}
			if (mode === "add") {
	  			$("#sortOrder").val(sortOrderForSelect[1]);
				$("#sortOrder").prop("disabled", true);
			}
  		}
  		
  		$("#deleteAddonToArticle").on("click", (e) => {
  			e.preventDefault();
  			let rowid = $("#articleAddonsGrid").jqGrid("getGridParam","selrow");
  			if( rowid != null ) {
  				let rowData = $("#articleAddonsGrid").jqGrid("getRowData",parseInt(rowid,10));
  				let idForDelUpdate = rowData["id"];
  				showMessageDialog("Delete selected record?", {
  					"type" : "warning",
  					"dialogId" : "confirmAddonToArticleDelete",
  					"buttons" : [ {
  						"id" : "add",
  						"type" : "good",
  						"text" : "Remove",
  						"callback" : () => {
  							deleteAddonToArticleOnServer(idForDelUpdate);	
  							$('#articleAddonsGrid').trigger( "reloadGrid" );
  						}
  					}, {
  						"id" : "cancel",
  						"type" : "bad",
  						"text" : "Cancel",
  						"callback" : () => {
  							hideMessageDialog("confirmAddonToArticleDelete");
  						}
  					} ]
  				});
  			} else {
  				showMessageDialog("Please select row!", {
  					"type" : "error"
  				});		
  			}
  		});
  		
  		function deleteAddonToArticleOnServer(rowId) {
  			hideMessageDialog("confirmAddonToArticleDelete");
  			showInProgessDialog();
  			
  			let postData = [];
  			postData.push({	
  		    	id: rowId
  		    });

  			$.ajax({
  				type :"POST",
  			    contentType :"application/json; charset=utf-8",
  			    dataType :"json",
  			    url: "PluginServlet?action=deleteArticleAddon",
  			    async: false,
  			    data: JSON.stringify(postData)
  			}).success((data) => {
  				hideInProgessDialog(); 	
  				showBackendCallStatus(data, "Addon to Article assigment deleted successfully!"); 
  				
//  				getArticleAddonsFromServer(ADDONARTICLESGROUPID);
  	  			resetGrid();
  			}).fail((xhr, status, error) => {
  				hideInProgessDialog();
  				showMessageDialog("Error in backend call: " + xhr.responseText, {
  					"type" : "error"
  				});		
  			});
  		}
  		
  		$("#editAddonToArticle").on("click", (e) => {
  			e.preventDefault();
  			let rowid = $("#articleAddonsGrid").jqGrid("getGridParam","selrow");
  			if( rowid != null ) {
  				$("#articleAddonsGrid").jqGrid("editGridRow",rowid,editAddonFormOptions);
  			} else {
  				showMessageDialog("Please select row!", {
  					"type" : "error"
  				});
  			}
  		});
  		
  		$("#addAddonToArticle").on("click", (e) => {
  			e.preventDefault();
			if (SELECTED_ADDON_GROUP_ITEM && SELECTED_ADDON_GROUP_ITEM != null && SELECTED_ADDON_GROUP_ITEM !== "") {  				
//				// load articles to edit select box
//	  			loadAddonArticles(); 
	  			$("#articleAddonsGrid").editGridRow("new", addAddonFormOptions);
  			} else {
  				showMessageDialog("Please select parent article!", {
  					"type" : "error"
  				});
  			}  			
  		});
  	}
     
    return {
    	createArticleAddonsGrid: createArticleAddonsGrid,
    	reloadArticleAddonsGrid: gridReload,
    	resetGrid: resetGrid
    };
}());
